﻿using Aponeo.Business.GH_Konditionen;
using log4net;
using System.Data;
using System.Linq;
using System.ServiceProcess;
using System.Management;
using System.Reflection;

namespace Aponeo.Services.GhKonditionenImporter
{
    public partial class AponeoGhKonditionenImporterService : ServiceBase
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private string _serviceName = null;
        private Worker _worker = null;

        /// <summary>
        /// Gets or sets the short name used to identify the service to the system.
        /// </summary>
        public new string ServiceName
        {
            get
            {
                if (_serviceName == null)
                {
                    _serviceName = GetServiceName();
                }
                return _serviceName;
            }
            private set { }
        }
        
        public AponeoGhKonditionenImporterService()
        {
            LogManager.GetLogger("initialise logging system");
            //InitializeComponent();

            CanPauseAndContinue = false;
        }

        protected override void OnStart(string[] args)
        {
            _log.Info($"---------- Starting Service {ServiceName} ----------");

            _worker = new Worker();
            _worker.RunEternalLoop();

            _log.Info($"---------- Service {ServiceName} started. ----------");
        }

        protected override void OnStop()
        {
            _log.Info($"---------- Stopping Service {ServiceName} ----------");

            // Stop worker
            if (_worker != null)
            {
                _worker.StopWork();
            }

            _log.Info($"---------- Service {ServiceName} stopped. ----------");
        }

        /// <summary>
        /// Gets the name of the service.
        /// </summary>
        /// <returns></returns>
        private string GetServiceName()
        {
            string serviceName = string.Empty;
            try
            {
                int processId = System.Diagnostics.Process.GetCurrentProcess().Id;
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_Service where ProcessId = " + processId);
                ManagementObjectCollection collection = searcher.Get();
                serviceName = (string)collection.Cast<ManagementBaseObject>().First()["Name"];
            }
            catch
            {
            }
            return serviceName;
        }

    }
}
