﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;

namespace Aponeo.Services
{
    /// <summary>
    /// Custom Project Installer
    /// </summary>
    /// <seealso cref="System.Configuration.Install.Installer" />
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProjectInstaller"/> class.
        /// </summary>
        public ProjectInstaller()
        {
            Initialize();
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        private void Initialize()
        {
            // ServiceProcessInstaller
            ServiceProcessInstaller spi = new ServiceProcessInstaller
            {
                Username = GetAppSetting("ServiceUser"),
                Password = GetAppSetting("ServicePassword"),
                Account = GetServiceAccountFromConfig(),
            };

            // ServiceInstaller
            ServiceInstaller si = new ServiceInstaller
            {
                ServiceName = GetAppSetting("ServiceName"),
                DisplayName = GetAppSetting("ServiceDisplayName"),
                Description = GetAppSetting("ServiceDescription"),
                StartType = GetStartTypeFromConfig(),
                DelayedAutoStart = true,
            };

            // ProjectInstaller
            this.Installers.AddRange(new System.Configuration.Install.Installer[] { spi, si });
        }

        #region AppSettings Stuff
        /// <summary>
        /// Gets the service account from configuration.
        /// </summary>
        /// <returns></returns>
        private ServiceAccount GetServiceAccountFromConfig()
        {
            ServiceAccount accountType = ServiceAccount.User;
            Enum.TryParse<ServiceAccount>(GetAppSetting("ServiceAccount") ?? "User", out accountType);
            return accountType;
        }

        /// <summary>
        /// Gets the start type from configuration.
        /// </summary>
        /// <returns></returns>
        private ServiceStartMode GetStartTypeFromConfig()
        {
            ServiceStartMode startType = ServiceStartMode.Manual;
            Enum.TryParse<ServiceStartMode>(GetAppSetting("ServiceStartMode") ?? "Automatic", out startType);
            return startType;
        }

        /// <summary>
        /// Gets the application setting.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        private string GetAppSetting(string name)
        {
            string value = null;

            try
            {
                var config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().Location);
                if (config.AppSettings.Settings.AllKeys.Contains(name))
                {
                    value = config.AppSettings.Settings[name].Value;
                }
            }
            catch (Exception ex)
            {
                LogMessage(ex);
            }
            return value;
        }
        #endregion

        #region Logging Stuff
        /// <summary>
        /// Gets the messages.
        /// </summary>
        /// <param name="ex">The ex.</param>
        /// <param name="includeStack">if set to <c>true</c> [include stack].</param>
        /// <returns></returns>
        private string GetMessages(Exception ex, bool includeStack = true)
        {
            string stack = ex.StackTrace;
            StringBuilder sb = new StringBuilder();
            while (ex != null)
            {
                sb.AppendLine(ex.Message);
                ex = ex.InnerException;
            }

            sb.AppendLine(stack);
            return sb.ToString();
        }

        /// <summary>
        /// Logs the message.
        /// </summary>
        /// <param name="ex">The ex.</param>
        private void LogMessage(Exception ex)
        {
            if (Context != null)
            {
                Context.LogMessage(GetMessages(ex));
            }
        }
        #endregion
    }
}
