﻿using log4net;
using System.Reflection;
using System.ServiceProcess;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace Aponeo.Services.GhKonditionenImporter
{
    static class Program
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        static void Main()
        {
            log4net.Config.XmlConfigurator.Configure();
            LogManager.GetLogger("initialise logging system");

            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new AponeoGhKonditionenImporterService()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
