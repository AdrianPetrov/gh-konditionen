﻿@ECHO OFF

:: get current working directory -- even when executing as admin
SET cwd=%~d0%~p0

:: get installutil.exe from latest framework version
SET framework=%WINDIR%\Microsoft.Net\Framework
FOR /F "tokens=* USEBACKQ" %%F IN (`DIR %framework%\v* /O:N /B`) DO (
	SET version=%%F
)
SET installutil="%framework%\%version%\installutil.exe"

:: TODO: use T4 template
SET service=GH Konditionen Service.exe

:: install service(s)
%installutil% /U "%cwd%%service%"

PAUSE