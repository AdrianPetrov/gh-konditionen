﻿using Aponeo.Basis;
using Aponeo.Basis.Mail;
using Aponeo.Business.GH_Konditionen.WholesalerList;
using Aponeo.Business.GH_Konditionen.DTOs;
using log4net;
using System;
using System.Collections.Generic;
using Aponeo.Business.GH_Konditionen.Configuration;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using System.Reflection;
using Aponeo.Logging.MailReportAppender;
using Aponeo.Tools.ExtensionMethods;

namespace Aponeo.Business.GH_Konditionen
{
    public class Worker
    {
        private const string SectionName = "importerConfig";
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private CancellationTokenSource _tokenSource;
        private List<WholesalerListImporterBase> _importer;
        private ImporterConfiguration _config = null;

        internal WorkerCache Cache { get; set; }

        internal WorkerCache UpdateCache()
        {
            if (Cache == null)
            {
                Cache = new WorkerCache();
            }
            Cache.LastRun = DateTime.Now;
            return Cache;
        }

        public bool IsFirstRunToday
        {
            get
            {
                return Cache == null || Cache.LastRun.Date < DateTime.Today;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is working.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is working; otherwise, <c>false</c>.
        /// </value>
        public bool IsWorking { get; private set; }

        private void CheckConfiguration()
        {
            CheckReasonableDiscount(_config.KehrDiscount, nameof(_config.KehrDiscount));
            CheckReasonableDiscount(_config.NowedaDiscount, nameof(_config.NowedaDiscount));
            CheckReasonableDiscount(_config.GeheGwcDiscount, nameof(_config.GeheGwcDiscount));
            CheckReasonableDiscount(_config.GeheBasicDiscount, nameof(_config.GeheBasicDiscount));
            CheckReasonableDiscount(_config.SanacorpDiscount, nameof(_config.SanacorpDiscount));
        }

        private void CheckReasonableDiscount(decimal pDiscount, string pParamName)
        {
            _log.Info($"{pParamName}={pDiscount}");
            if (pDiscount > 1 || pDiscount < 0.9m)
            {
                throw new ConfigurationErrorsException($"unreasonable discount '{pDiscount}' for parameter '{pParamName}'");
            }
        }

        /// <summary>
        /// Runs every X minutes within an eternal loop until the work is cancelled or stopped from outside via <see cref="StopWork"/>
        /// </summary>
        public void RunEternalLoop()
        {
            // Initialize the cancellation token.
            _tokenSource = new CancellationTokenSource();
            CancellationToken token = _tokenSource.Token;

            Cache = new WorkerCache();

            Task t = Task.Run(() => {
                IsWorking = true;

                // Poll cancellation token
                while (!token.IsCancellationRequested)
                {
                    RunOnes();

                    // Wait some time ()
                    int wait = _config?.ScheduleInterval ?? 1;
                    _log.Info($"Sleeping for {wait} minutes...");
                    token.WaitHandle.WaitOne(wait * 1000 * 60);
                }

                IsWorking = false;
                _log.Info($"Work cancelled.");
            }, token);

            _log.Info($"Work started.");
        }

        /// <summary>
        /// Stops the work which was initiated via <see cref="RunEternalLoop"/>
        /// </summary>
        public void StopWork()
        {
            _log.Info("Stopping worker execution...");

            if (_tokenSource != null)
            {
                _tokenSource.Cancel();

                // wait for worker to finish its execution
                while (IsWorking)
                {
                    Thread.Sleep(100);
                }
            }

            _log.Info("Worker execution stopped.");
        }

        /// <summary>
        /// Runs ones all importers and terminates afterwards
        /// </summary>
        public void RunOnes()
        {
            _log.Info("run");

            try
            {
                bool configOk = Initialize();
                if (!configOk)
                {
                    _log.Info("Initialization failed");
                    _log.Debug(MailReportAppender._flushCmd);
                    return;
                }

                DatabaseHelper.ConnectionString = _config.ConnectionString;

                // check which importer has to run
                Dictionary<WholesalerListImporterBase, List<DtoConditionFileEntry>> importerToRun = GetImportersToRun(_importer);

                if (importerToRun.Count > 0)
                {
                    _log.Info("run all importers");

                    // save the current state of best prices before importing anything
                    List<DtoBestPrice> bestPricesBefore = DtoBestPrice.GetAllBestPrices().ToList();
                    List<DtoBestPrice> bestPricesAfter = null;

                    // run all importers
                    Dictionary<WholesalerListImporterBase, DtoConditionFileEntry> changedConditinons = RunImporters(importerToRun);

                    // only do next steps, if the importers have done sth.
                    if (changedConditinons.Count > 0)
                    {
                        // if the importers really have imported sth., then merge the new conditions
                        bool filesImported = changedConditinons.Count(c => c.Value.LastImportedAt != null) > 0;
                        if (filesImported)
                        {
                            WholesalerListImporterBase.MergeNewConditions();
                            bestPricesAfter = DtoBestPrice.GetAllBestPrices().ToList();
                        }

                        // create reports
                        string mailTxt = CreateMailReport(changedConditinons, filesImported, bestPricesBefore, bestPricesAfter);

                        // different recipients if only reimported everything
                        string mailRecipients = changedConditinons.Any(c => c.Value.LastImportedAt == c.Value.ImportedAt) ? _config.MailRecipients : _config.MailRecipientsReimport;

                        string subject = Constants.MAIL_SUBJECT + _config.MailSubjectSuffix;

                        new DatabaseMailSender().Send(Constants.MAIL_SENDER_NAME, Constants.MAIL_SENDER_ADDRESS, mailRecipients, subject, mailTxt, _config.ConnectionStringMail);
                    }
                }
                else
                {
                    _log.Info("nothing to do");
                }

                UpdateCache();
            }
            catch (Exception ex)
            {
                _log.Error("analysing and/or import failed", ex);
            }
            finally
            {
                _log.Debug(MailReportAppender._flushCmd);
            }
        }

        private bool Initialize()
        {
            try
            {
                // Get configuration from ConfigSection in app.config
                ConfigurationManager.RefreshSection(SectionName);
                _config = ConfigurationManager.GetSection(SectionName) as ImporterConfiguration;
                CheckConfiguration();

                // get all available importers via reflection
                _importer = new List<WholesalerListImporterBase>();
                Assembly assembly = typeof(Worker).GetTypeInfo().Assembly;
                foreach (Type t in assembly.GetTypes().Where(t => t.IsSubclassOf(typeof(WholesalerListImporterBase)) && !t.Name.Equals(nameof(AepImporter))))
                {
                    _importer.Add(Activator.CreateInstance(t, _config) as WholesalerListImporterBase);
                }
                return true;
            }
            catch (Exception ex)
            {
                _log.Error("config initialization failed", ex);
                return false;
            }
        }

        private Dictionary<WholesalerListImporterBase, List<DtoConditionFileEntry>> GetImportersToRun(List<WholesalerListImporterBase> pImporters)
        {
            Dictionary<WholesalerListImporterBase, List<DtoConditionFileEntry>> result = new Dictionary<WholesalerListImporterBase, List<DtoConditionFileEntry>>();

            bool isDayOfAbdaUpdate = DateTime.Today.Day == 1 || DateTime.Today.Day == 15;
            if (IsFirstRunToday && isDayOfAbdaUpdate)
            {
                DtoConditionFileEntry.PrepareLastConditionFileEntriesForReimport();
            }

            foreach (WholesalerListImporterBase imp in pImporters)
            {
                List<DtoConditionFileEntry> newFilesToProcess = imp.GetNewFilesToProcess();

                if (newFilesToProcess?.Count > 0)
                {
                    result.Add(imp, newFilesToProcess);
                }
            }

            return result;
        }

        private static Dictionary<WholesalerListImporterBase, DtoConditionFileEntry> RunImporters(Dictionary<WholesalerListImporterBase, List<DtoConditionFileEntry>> pImporters)
        {
            Dictionary<WholesalerListImporterBase, DtoConditionFileEntry> result = new Dictionary<WholesalerListImporterBase, DtoConditionFileEntry>();

            foreach (KeyValuePair<WholesalerListImporterBase, List<DtoConditionFileEntry>> imp in pImporters)
            {
                DtoConditionFileEntry changedConditions = RunImporter(imp.Key, imp.Value);
                if (changedConditions != null && changedConditions.CheckedAt != null)
                {
                    // the file was at least analysed => save it for later report
                    result.Add(imp.Key, changedConditions);
                }
            }           

            return result;
        }

        private static DtoConditionFileEntry RunImporter(WholesalerListImporterBase pImporter, List<DtoConditionFileEntry> pNewFilesToProcess)
        {
            DtoConditionFileEntry changedConditions = null;

            if (pNewFilesToProcess?.Count > 0)
            {
                changedConditions = pImporter.Analyse(pNewFilesToProcess);
                if (changedConditions != null && changedConditions.CheckedAt != null)
                {
                    if (changedConditions.HasToBeImported())
                    {
                        pImporter.Import(changedConditions);
                    }
                    else
                    {
                        _log.Notice($"file {changedConditions.FileName} will be imported at {changedConditions.ImportDate.ToString("dd.MM.yyyy HH:mm")}");
                    }
                }
                else
                {
                    _log.Error($"some files of {pImporter.SharePointSubfolder} needed to be processed but no result was returned");
                }
            }
            else
            {
                _log.Warn($"importer should not run");
            }

            // TODO: check if files are missing but should be there (by statistics)

            return changedConditions;
        }
        
        private static string CreateMailReport(Dictionary<WholesalerListImporterBase, DtoConditionFileEntry> pChangedConditinons, 
                                               bool pFilesImported, 
                                               List<DtoBestPrice> pBestPricesBefore, 
                                               List<DtoBestPrice> pBestPricesAfter)
        {
            string listItems = GetChangedConditionInfos(pChangedConditinons);
            string bestPriceComparison = GetBestPriceComparison(pFilesImported, pBestPricesBefore, pBestPricesAfter);

            return String.Format(Constants.MAIL_MSG_TXT, Constants.HTML_TABLE_STYLE_HEADER, listItems, bestPriceComparison);
        }

        private static string GetBestPriceComparison(bool pFilesImported,
                                                     List<DtoBestPrice> pBestPricesBefore,
                                                     List<DtoBestPrice> pBestPricesAfter)
        {
            string result = "";

            if (pFilesImported)
            {
                List<string> listItems = new List<string>
                {
                    GetHtmlTableLine(new List<string>() { "Liste", "# Bestpreise vorher", "# Bestpreise nachher", "&#916;", "%" }, true)
                };

                foreach (DtoBestPrice bp in pBestPricesBefore)
                {
                    DtoBestPrice bpa = pBestPricesAfter.SingleOrDefault(b => b.WholesalerListName == bp.WholesalerListName);
                    int diff = (bpa == null ? 0 : bpa.BestPrices) - bp.BestPrices;
                    decimal diffPercentage = bp.BestPrices > 0 ? (decimal)diff / bp.BestPrices : 0;

                    listItems.Add(GetHtmlTableLine(new List<string>() {
                                                    bp.WholesalerListName,
                                                    bp.BestPrices.ToString(Constants.NUMBER_FORMAT),
                                                    bpa.BestPrices.ToString(Constants.NUMBER_FORMAT),
                                                    diff.ToString("+#,##0;-#,##0"),
                                                    diffPercentage.ToString("+#0.0%;-#0.0%"),
                                                   }));
                }
                result = GetFormattedHtmlTable(listItems);
            }

            return result;
        }

        private static string GetChangedConditionInfos(Dictionary<WholesalerListImporterBase, DtoConditionFileEntry> pChangedConditions)
        {
            // header
            List<string> listItems = new List<string>
            {
                GetHtmlTableLine(new List<string>() { "Liste", "Datei", "geprüft am", "wird import am", "wurde importiert am", "# Gesamt", "# Preisänderungen", "Ø &#916; EK", "Link zu Änderungen" }, true)
            };

            // lines
            foreach (KeyValuePair<WholesalerListImporterBase, DtoConditionFileEntry> kvp in pChangedConditions)
            {
                WholesalerListImporterBase imp = kvp.Key;
                DtoConditionFileEntry cfe = kvp.Value;

                // total number of prices
                int cntTotal = cfe.ConditionComparison.NumberNew;

                // Ø Δ price changes
                decimal sumDiff = cfe.ConditionComparison.Changes.Sum(c => (c.Value.Price ?? 0) - (c.Key.Price ?? 0));
                decimal sumOld = cfe.ConditionComparison.Changes.Sum(c => c.Key.Price ?? 0);
                decimal averageDelta = sumOld > 0 ? sumDiff / sumOld : 0;

                // number of price changes
                int cntDiff = cfe.ConditionComparison.Changes.Count();

                listItems.Add(GetHtmlTableLine(new List<string>() {
                                                   imp.SharePointSubfolder,
                                                   cfe.FileName,
                                                   ((DateTime)cfe.CheckedAt).ToString(Constants.DATE_FORMAT),
                                                   cfe.LastImportedAt == null ? cfe.ImportDate.ToString(Constants.DATE_FORMAT) : "",
                                                   cfe.LastImportedAt == null ? "" : ((DateTime)cfe.LastImportedAt).ToString(Constants.DATE_FORMAT),
                                                   cntTotal.ToString(Constants.NUMBER_FORMAT),
                                                   $"{cntDiff.ToString(Constants.NUMBER_FORMAT)} ({(cntTotal > 0 ? (decimal)cntDiff / cntTotal: 0):#0.0%})",
                                                   averageDelta.ToString("+#0.0%;-#0.0%"),
                                                   cfe.UrlToChanges
                                               }));
            }

            return GetFormattedHtmlTable(listItems);
        }

        private static string GetHtmlTableLine(IEnumerable<string> cols, bool pIsHeader = false)
        {
            string line = "";

            foreach (string col in cols)
            {
                line += $"<td>{col}</td>";
            }
            line = $"<tr>{line}</tr>";

            if (pIsHeader)
            {
                line = $"<thead>{line}</thead>";
            }

            return line;
        }

        private static string GetFormattedHtmlTable(List<string> pTableLines)
        {
            string linesStr = "";

            bool odd = true;
            foreach (string line in pTableLines)
            {
                odd = !odd;
                linesStr += line.Replace("<tr>", String.Format(@"<tr style=""background-color:{0}"">", odd ? Constants.COLOR_TABLE_ODD_ROW : Constants.COLOR_TABLE_EVEN_ROW));
            }

            return String.Format(Constants.HTML_TABLE, linesStr);
        }

    }
}
