﻿namespace Aponeo.Business.GH_Konditionen
{
    public class Constants
    {
        public const int FORECAST_HOURS_TO_IMPORT = 8;

        public const string NUMBER_FORMAT = "#,##0";
        public const string DECIMAL_FORMAT = "#,##0.00";
        public const string CURRENCY_FORMAT = "#,##0.00 €";
        public const string PERCENTAGE_FORMAT = "#0.00%";
        public const string DATE_FORMAT = "dd.MM.yyyy HH:mm";
        public const string DATE_REGEX = "[0-9]{4}.[0-9]{2}.[0-9]{2}";
        public const string FILENAME_DATE_PATTERN = "yyyy.MM.dd";
        
        // Mail
        public const string MAIL_SENDER_NAME = "GH Konditionen Importer";
        public const string MAIL_SENDER_ADDRESS = "GhKonditionenService@aponeo.de";
        public const string MAIL_SUBJECT = "Verarbeitete Konditionen";
        public const string MAIL_MSG_TXT =
@"<html>

<head>
{0}
</head>

<body>

{1}
<br/>
<br/>
<br/>
{2}

</body></html>
";

        public const string HTML_TABLE = @"<table id=""table_style"">{0}</table>";

        public const string HTML_TABLE_STYLE_HEADER =
        @"<style>
	        #table_style {
	          font-family: Arial, Helvetica, sans-serif;
	          border: 1px solid #020126;
	          background-color: #FFFFFF;
	          width: 100%;
	          text-align: left;
	          border-collapse: collapse;
	        }
	        #table_style td {
	          border: 1px solid #020126;
	          padding: 3px 2px;
	        }
	        #table_style tbody td {
	          font-size: 12px;
	          font-weight: bold;
	          color: #020126;
	        }
	        #table_style thead td {
	          background: #595959;
	          border-bottom: 1px solid #020126;

	          font-size: 16px;
	          font-weight: normal;
	          color: #FFFFFF;
	          text-align: left;
	        }
        </style>";

        public const string COLOR_TABLE_ODD_ROW = "#ffffff";
        public const string COLOR_TABLE_EVEN_ROW = "#e2e2e4";

    }

}
