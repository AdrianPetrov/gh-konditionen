﻿using System;
using System.Text;
using System.Collections.Generic;
using log4net;
using log4net.Appender;
using log4net.Core;
using System.Linq;
using Aponeo.Basis;

namespace Aponeo.Logging
{
    public class MailReportAppender : AppenderSkeleton, IBufferAppender
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string MailSenderName { get; set; }
        public string MailSenderAddress { get; set; }
        public string Recipients { get; set; }
        public int MaxMsgSize { get; set; }
        public string MailSenderType { get; set; }

        private static Dictionary<Level, List<string>> _msgBuffer = new Dictionary<Level, List<string>>();
        private int CurrentMsgBufferSize
        {
            get
            {
                int result = 0;

                foreach (Level lvl in _msgBuffer.Keys)
                {
                    result += _msgBuffer[lvl].Count;
                }

                return result;
            }
        }

        protected override void Append(LoggingEvent loggingEvent)
        {
            AddMsgToBuffer(loggingEvent.Level, loggingEvent.RenderedMessage);
        }

        public void ClearMsgBuffer()
        {
            if (_msgBuffer != null)
            {
                _msgBuffer.Clear();
            }
        }


        public void Flush()
        {
            Flush(null);
        }

        public void Flush(string pSubject = null)
        {
            if (CurrentMsgBufferSize > 0 && !String.IsNullOrEmpty(MailSenderType))
            {
                try
                {
                    Type objectType = Type.GetType(MailSenderType);
                    if (objectType == null)
                    {
                        _log.Error($"no objectType {MailSenderType} available");
                    }
                    else
                    {
                        IMailSender mailSender = Activator.CreateInstance(objectType) as IMailSender;

                        if (String.IsNullOrEmpty(pSubject))
                        {
                            pSubject = GetEmailSubject();
                        }

                        string body = GetEmailBody(MaxMsgSize);

                        // Set destinations for the e-mail message.
                        foreach (string address in Recipients.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            mailSender.Send(MailSenderName, MailSenderAddress, address, pSubject, body);
                        }

                        ClearMsgBuffer();
                    }
                }
                catch (Exception e)
                {
                    _log.Error($"failed to send mail via {MailSenderType}", e);
                }

            }
        }

        public string GetEmailBody(int pMaxSizeLogEmailMsg)
        {
            StringBuilder msgBuilder = new StringBuilder();
            int currentSizeMsg = 0;

            // get the first maxSizeLogEmailMsg messages ordered by their level (first errors, then warnings, then infos)
            List<Level> levels = new List<Level>(_msgBuffer.Keys);

            bool breakLoop = false;
            foreach (Level lvl in levels.OrderByDescending(l => l.Value))
            {
                if (breakLoop)
                {
                    break;
                }

                foreach (string msg in _msgBuffer[lvl])
                {
                    if (breakLoop)
                    {
                        break;
                    }

                    if (currentSizeMsg < pMaxSizeLogEmailMsg)
                    {
                        msgBuilder.AppendLine(lvl + ": " + msg);
                    }
                    else if (currentSizeMsg == pMaxSizeLogEmailMsg)
                    {
                        msgBuilder.AppendLine("...");
                        msgBuilder.AppendLine(String.Format("--> {0} messages removed", CurrentMsgBufferSize - pMaxSizeLogEmailMsg));
                        breakLoop = true;
                    }
                    currentSizeMsg++;
                }
            }

            return msgBuilder.ToString();
        }

        public void AddMsgToBuffer(Level pLevel, string pMessage)
        {
            if (!_msgBuffer.ContainsKey(pLevel))
            {
                _msgBuffer.Add(pLevel, new List<string>());
            }
            _msgBuffer[pLevel].Add(pMessage);
        }

        private string GetEmailSubject()
        {
            string result = "";

            Level maxLevel = _msgBuffer.Keys.Max();

            if (maxLevel != null)
            {
                result = maxLevel.ToString() + ": " + _msgBuffer[maxLevel].Count;
            }
            else
            {
                result = "UNKNOWN";
            }

            return result;
        }
    }

}
