﻿using log4net;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Aponeo.Basis
{
    public class DatabaseMailSender : IMailSender
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void Send(string pSenderName, string pSenderAddress, string pRecipient, string pSubject, string pBody)
        {
            _log.Debug("insert mail to database to be sent");

            string cmdTxt = @"
                    INSERT INTO [Mail] ([User_ID], [Email_Header], [Email_Body], [Kunden_ID], [Beleg_ID], [Absender_Adresse], [Empfänger_Adresse], [Absender_Name], [Empfänger_Name], [Organisation], [Zeitstempel])
                    VALUES  (3
                            ,@subject
                            ,@body
                            ,-1
                            ,-1
                            ,@senderMail
                            ,@recipientMail
                            ,@senderName
                            ,@recipientMail
                            ,'Aponeo'
                            ,GETDATE()
                            )
                ";

            // Set destinations for the e-mail message.
            List<SqlParameter> sqlParameters = new List<SqlParameter>
                {
                    new SqlParameter("@senderName", pSenderName),
                    new SqlParameter("@senderMail", pSenderAddress),
                    new SqlParameter("@recipientMail", pRecipient),
                    new SqlParameter("@subject", pSubject),
                    new SqlParameter("@body", pBody),
                };

            DatabaseHelper.ExecuteNonQuery(cmdTxt, sqlParameters);
        }
    }
}
