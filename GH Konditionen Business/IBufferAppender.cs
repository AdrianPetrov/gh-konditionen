﻿using log4net.Core;

namespace Aponeo.Logging
{
    public interface IBufferAppender
    {
        void AddMsgToBuffer(Level pLevel, string pMessage);
        void Flush();
        void ClearMsgBuffer();
    }
}
