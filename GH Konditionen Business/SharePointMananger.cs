﻿using Aponeo.Business.GH_Konditionen.Configuration;
using Aponeo.Business.GH_Konditionen.DTOs;
using Aponeo.SharePoint;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SpoFile = Microsoft.SharePoint.Client.File;

namespace Aponeo.Business.GH_Konditionen
{
    public class SharePointManager
    {
        private readonly string SharePointMainFolder;
        private readonly string SharePointMainFolderChanges;

        private readonly SharePointHelper SharePointHelper;

        public SharePointManager(ImporterConfiguration pConfig)
        {
            SharePointMainFolder = pConfig.SharePointMainFolder;
            SharePointMainFolderChanges = pConfig.SharePointMainFolderChanges;
            SharePointHelper = new SharePointHelper(pConfig.SharePointSite, pConfig.SharePointUser, pConfig.SharePointPassword, pConfig.SharePointRootFolder);
        }

        public List<SpoFile> GetAllAvailableFiles(string pSharePointSubfolder)
        {
            List<SpoFile> result = new List<SpoFile>();

            string sharePointSubfolder = GetListSubFolder(SharePointMainFolder, pSharePointSubfolder);
            result = SharePointHelper.GetFileList(sharePointSubfolder).OrderBy(f => f.Name).ToList();

            return result;
        }

        public StreamReader GetStreamReader(DtoConditionFileEntry pConditionFileEntry, string pSharePointSubfolder)
        {
            StreamReader result;

            string sharePointSubfolder = GetListSubFolder(SharePointMainFolder, pSharePointSubfolder);
            result = SharePointHelper.GetFileStreamReader(sharePointSubfolder, pConditionFileEntry.FileName);

            return result;
        }

        public string Upload(string pSharePointSubfolder, byte[] pBytes, string pFilename)
        {
            string sharePointSubfolder = GetListSubFolder(SharePointMainFolderChanges, pSharePointSubfolder);

            using (MemoryStream ms = new MemoryStream(pBytes))
            {
                string url = SharePointHelper.UploadStream(sharePointSubfolder, ms, pFilename, true);
                return url;
            }
        }

        private static string GetListSubFolder(string pMainFolder, string pSharePointSubfolder)
        {
            return $"{pMainFolder}/{pSharePointSubfolder}";
        }

    }
}
