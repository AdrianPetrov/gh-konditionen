﻿using System.Xml.Serialization;

namespace Aponeo.Business.GH_Konditionen.Configuration
{
    public class ImporterConfiguration
    {
        /// <summary>
        /// The interval in minutes how often the importer runs.
        /// </summary>
		[XmlElement("ScheduleInterval")]
        public int ScheduleInterval { get; set; }

        /// <summary>
        /// Gets or sets the database connection string.
        /// </summary>
		[XmlElement("ConnectionString")]
        public string ConnectionString { get; set; }

        /// <summary>
        /// Gets or sets the database connection string for mail sending.
        /// </summary>
		[XmlElement("ConnectionStringMail")]
        public string ConnectionStringMail { get; set; }

        /// <summary>
        /// Gets the recipient list (";" separated) where the mails should be send to.
        /// </summary>
		[XmlElement("MailRecipients")]
        public string MailRecipients { get; set; }

        /// <summary>
        /// Gets the recipient list (";" separated) where the mails should be send to if all list were reimported
        /// </summary>
		[XmlElement("MailRecipientsReimport")]
        public string MailRecipientsReimport { get; set; }

        /// <summary>
        /// An suffix which will be appended to the mail subject after import
        /// </summary>
		[XmlElement("MailSubjectSuffix")]
        public string MailSubjectSuffix { get; set; }

        /// <summary>
        /// The declaration if the upload to SharePoint should be done.
        /// </summary>
		[XmlElement("SharePointUpload")]
        public bool SharePointUpload { get; set; }

        /// <summary>
        /// The user to access SharePoint.
        /// </summary>
		[XmlElement("SharePointUser")]
        public string SharePointUser { get; set; }

        /// <summary>
        /// The password to access SharePoint.
        /// </summary>
		[XmlElement("SharePointPassword")]
        public string SharePointPassword { get; set; }

        /// <summary>
        /// The SharePoint website.
        /// </summary>
		[XmlElement("SharePointSite")]
        public string SharePointSite { get; set; }

        /// <summary>
        /// The root subfolder in SharePoint for all documents.
        /// </summary>
		[XmlElement("SharePointRootFolder")]
        public string SharePointRootFolder { get; set; }

        /// <summary>
        /// The main subfolder in SharePoint where the condition files can be downloaded from.
        /// </summary>
		[XmlElement("SharePointMainFolder")]
        public string SharePointMainFolder { get; set; }

        /// <summary>
        /// The main subfolder in SharePoint where the changes in condition files can be uploaded to.
        /// </summary>
		[XmlElement("SharePointMainFolderChanges")]
        public string SharePointMainFolderChanges { get; set; }

        /// <summary>
        /// The hours a file has to be imported before it gets valid.
        /// </summary>
		[XmlElement("HoursToImportBeforeValid")]
        public int HoursToImportBeforeValid { get; set; }

        /// <summary>
        /// If a file gets valid at Sunday or Monday, then the import should be done in both cases on Saturday (!) at a certain time. 
        /// The idea is that the prices should be calculated from Saturday on on the basis of the new conditions, since the first wholesaler order will be done on Monday when the new conditions are valid.
        /// </summary>
		[XmlElement("HourToImportAtSaturday")]
        public int HourToImportAtSaturday { get; set; }

        /// <summary>
        /// The discount we get from Kehr.
        /// </summary>
		[XmlElement("KehrDiscount")]
        public decimal KehrDiscount { get; set; }

        /// <summary>
        /// The discount we get from Noweda.
        /// </summary>
		[XmlElement("NowedaDiscount")]
        public decimal NowedaDiscount { get; set; }

        /// <summary>
        /// The discount we get from Noweda.
        /// </summary>
		[XmlElement("NowedaDiscountExceptionList")]
        public string NowedaDiscountExceptionList { get; set; }

        /// <summary>
        /// The discount we get from GEHE for GWC.
        /// </summary>
		[XmlElement("GeheGwcDiscount")]
        public decimal GeheGwcDiscount { get; set; }

        /// <summary>
        /// The discount we get from GEHE for Basic.
        /// </summary>
		[XmlElement("GeheBasicDiscount")]
        public decimal GeheBasicDiscount { get; set; }

        /// <summary>
        /// The discount we get from Sanacorp.
        /// </summary>
		[XmlElement("SanacorpDiscount")]
        public decimal SanacorpDiscount { get; set; }

    }
}
