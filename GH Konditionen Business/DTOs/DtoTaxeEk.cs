﻿using Aponeo.Basis;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;

namespace Aponeo.Business.GH_Konditionen.DTOs
{
    public class DtoTaxeEk
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public int Pzn { get; private set; }
        public decimal TaxePurchasePrice { get; private set; }

        public DtoTaxeEk(int pPzn, decimal pTaxePurchasePrice)
        {
            Pzn = pPzn;
            TaxePurchasePrice = pTaxePurchasePrice;
        }

        public static DtoTaxeEk Create(DataRow pDataRow)
        {
            try
            {
                return new DtoTaxeEk(pDataRow.GetValueOrDefault<int>("PZN"),
                                     pDataRow.GetValueOrDefault<decimal>("Einkaufspreis")
                                     );
            }
            catch (Exception e)
            {
                _log.Error($"could not parse {typeof(DtoTaxeEk).Name}", e);
                return null;
            }
        }

        public static IEnumerable<DtoTaxeEk> GetTaxeEks()
        {
            List<DtoTaxeEk> result = new List<DtoTaxeEk>();

            _log.Info("get TaxeEKs");

            string cmdTxt = @"
                SELECT ai.PZN, ap.Einkaufspreis
                  FROM ArtikelstammPreis ap
                        INNER JOIN ArtikelstammIdentifikation ai
                            ON ai.Artikelnummer = ap.Artikelnummer
            ";

            using (var dt = DatabaseHelper.ExecuteDataTable(cmdTxt))
            {
                foreach (DataRow dr in dt.Rows)
                {
                    DtoTaxeEk item = Create(dr);
                    if (item != null)
                    {
                        result.Add(item);
                    }
                }
            }

            return result;
        }

    }
}
