﻿using Aponeo.Basis;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Aponeo.Business.GH_Konditionen.DTOs
{
    public class DtoCondition : ICloneable
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// WaWiArtikelbezugKonditionenBasis.ID
        /// </summary>
        public long ID { get; private set; }

        /// <summary>
        /// WaWiArtikelbezugKonditionenBasis.ArtikelbezugKonditionenPriorisierungID
        /// </summary>
        public int PrioId { get; private set; }

        /// <summary>
        /// WaWiArtikelbezugKonditionenBasis.PZN
        /// </summary>
        public int Pzn { get; private set; }

        /// <summary>
        /// WaWiArtikelbezugKonditionenBasis.Einkaufspreis
        /// </summary>
        public decimal? Price { get; private set; }

        /// <summary>
        /// WaWiArtikelbezugKonditionenBasis.Rabatt
        /// </summary>
        public decimal? Discount { get; private set; }

        /// <summary>
        /// ArtikelstammPreis.Einkaufspreis
        /// </summary>
        public decimal? TaxePurchasePrice { get; private set; }

        /// <summary>
        /// ArtikelstammName.Anzeigename
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Creator
        /// </summary>
        /// <param name="pPrioId">The WaWiArtikelbezugKonditionenBasis.ArtikelbezugKonditionenPriorisierungID</param>
        /// <param name="pPzn">The PZN</param>
        /// <param name="pPrice">The price in Euro</param>
        public DtoCondition(int pPrioId, int pPzn, decimal? pPrice, decimal? pDiscount = null)
        {
            Init(pPrioId, pPzn, pPrice, pDiscount);
        }

        /// <summary>
        /// Creator
        /// </summary>
        /// <param name="pID">The WaWiArtikelbezugKonditionenBasis.ID</param>
        /// <param name="pPrioId">The WaWiArtikelbezugKonditionenBasis.ArtikelbezugKonditionenPriorisierungID</param>
        /// <param name="pPzn">The PZN</param>
        /// <param name="pPrice">The price in Euro</param>
        public DtoCondition(long pID, int pPrioId, int pPzn, decimal? pPrice, decimal? pDiscount = null)
        {
            ID = pID;
            Init(pPrioId, pPzn, pPrice, pDiscount);
        }

        private void Init(int pPrioId, int pPzn, decimal? pPrice, decimal? pDiscount)
        {
            PrioId = pPrioId;
            Pzn = pPzn;
            // the database type is MONEY => precision 4
            Price = pPrice == null ? pPrice : Math.Round((decimal)pPrice, 4);
            Discount = pDiscount;
        }

        public static IEnumerable<DtoCondition> GetConditions(int pPrioId)
        {
            List<DtoCondition> result = new List<DtoCondition>();

            _log.Info("get conditions");

            string cmdTxt = @"
                SELECT [ID]
                      ,[PZN]
                      ,[Einkaufspreis]
                      ,[Rabatt]
                      ,[ArtikelbezugKonditionenPriorisierungID]
                  FROM [WaWiArtikelbezugKonditionenBasis]
                 WHERE [ArtikelbezugKonditionenPriorisierungID] = @ArtikelbezugKonditionenPriorisierungID
            ";

            List<SqlParameter> sqlParameters = new List<SqlParameter>
            {
                new SqlParameter("@ArtikelbezugKonditionenPriorisierungID", pPrioId),
            };

            using (var dt = DatabaseHelper.ExecuteDataTable(cmdTxt, sqlParameters))
            {
                foreach (DataRow record in dt.Rows)
                {
                    DtoCondition item = Create(record);
                    if (item != null)
                    {
                        result.Add(item);
                    }
                }
            }

            return result;
        }

        private static DtoCondition Create(DataRow pDataRow)
        {
            try
            {
                return new DtoCondition(pDataRow.GetValueOrDefault<long>("ID"),
                                        pDataRow.GetValueOrDefault<int>("ArtikelbezugKonditionenPriorisierungID"),
                                        pDataRow.GetValueOrDefault<int>("PZN"),
                                        pDataRow.GetValueOrDefault<decimal?>("Einkaufspreis"),
                                        pDataRow.GetValueOrDefault<decimal?>("Rabatt")
                                        );
            }
            catch (Exception e)
            {
                _log.Error($"could not parse {typeof(DtoCondition).Name}", e);
                return null;
            }
        }

        public static IEnumerable<DtoCondition> AddAdditionalInformation(IEnumerable<DtoCondition> pConditions)
        {
            Dictionary<int, DtoCondition> conditionDict = pConditions.ToDictionary(e => e.Pzn, e => e);

            _log.Info("add additional information to conditions");

            string cmdTxt = @"
                SELECT ai.PZN, an.Anzeigename, ap.Einkaufspreis
                  FROM ArtikelstammPreis ap
                        INNER JOIN ArtikelstammIdentifikation ai
                            ON ai.Artikelnummer = ap.Artikelnummer
                        INNER JOIN ArtikelstammName an
                            ON an.Artikelnummer = ai.Artikelnummer
            ";

            using (var dt = DatabaseHelper.ExecuteDataTable(cmdTxt))
            {
                foreach (DataRow dr in dt.Rows)
                {
                    int pzn = dr.GetValueOrDefault<int>("PZN");

                    if (conditionDict.ContainsKey(pzn))
                    {
                        DtoCondition condition = conditionDict[pzn];

                        condition.TaxePurchasePrice = dr.GetValueOrDefault<decimal>("Einkaufspreis");
                        condition.Name = dr.GetValueOrDefault<string>("Anzeigename");
                    }
                }
            }

            foreach (DtoCondition c in pConditions.Where(c => String.IsNullOrEmpty(c.Name)))
            {
                _log.Info($"PZN {c.Pzn} not yet known");
            }

            return conditionDict.Values;
        }

        public void Insert()
        {
            string cmdTxt = @"
                INSERT INTO WaWiArtikelbezugKonditionenBasis (ArtikelbezugKonditionenPriorisierungID, PZN, Einkaufspreis, Rabatt)
                VALUES (@ArtikelbezugKonditionenPriorisierungID, @PZN, @Einkaufspreis, @Rabatt)
            ";

            List<SqlParameter> sqlParameters = new List<SqlParameter>
            {
                new SqlParameter("@ArtikelbezugKonditionenPriorisierungID", PrioId),
                new SqlParameter("@PZN", Pzn),
                new SqlParameter("@Einkaufspreis", Price),
                new SqlParameter("@Rabatt", Discount),
            };

            DatabaseHelper.ExecuteNonQuery(cmdTxt, sqlParameters);
        }

        public void Delete()
        {
            string cmdTxt = @"
                DELETE FROM WaWiArtikelbezugKonditionenBasis 
                 WHERE ID = @ID
            ";

            List<SqlParameter> sqlParameters = new List<SqlParameter>
            {
                new SqlParameter("@ID", ID),
            };

            DatabaseHelper.ExecuteNonQuery(cmdTxt, sqlParameters);
        }

        public void Update(decimal? pPrice, decimal? pDiscount)
        {
            string cmdTxt = @"
                UPDATE WaWiArtikelbezugKonditionenBasis 
                   SET Einkaufspreis = @Einkaufspreis,
                       Rabatt = @Rabatt
                 WHERE ID = @ID
            ";

            List<SqlParameter> sqlParameters = new List<SqlParameter>
            {
                new SqlParameter("@ID", ID),
                new SqlParameter("@Einkaufspreis", pPrice),
                new SqlParameter("@Rabatt", pDiscount),
            };

            DatabaseHelper.ExecuteNonQuery(cmdTxt, sqlParameters);
        }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
