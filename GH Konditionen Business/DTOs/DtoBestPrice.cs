﻿using Aponeo.Basis;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;

namespace Aponeo.Business.GH_Konditionen.DTOs
{
    public class DtoBestPrice
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string WholesalerListName { get; private set; }
        public int BestPrices{ get; private set; }

        public DtoBestPrice(string pWholesalerListName, int pBestPrices)
        {
            WholesalerListName = pWholesalerListName;
            BestPrices = pBestPrices;
        }

        public static DtoBestPrice Create(DataRow pDataRow)
        {
            try
            {
                return new DtoBestPrice(pDataRow.GetValueOrDefault<string>("ListeName"),
                                        pDataRow.GetValueOrDefault<int>("Cnt")
                                        );
            }
            catch (Exception e)
            {
                _log.Error($"could not parse {typeof(DtoBestPrice).Name}", e);
                return null;
            }
        }

        public static IEnumerable<DtoBestPrice> GetAllBestPrices()
        {
            List<DtoBestPrice> result = new List<DtoBestPrice>();

            _log.Info("get the number of best prices per wholesaler list");

            string cmdTxt = @"
                SELECT lp.ListeName
                      ,COUNT(1) AS Cnt
                  FROM ViewWaWiArtikelbezugLieferantenPrioritaet lp
                        INNER JOIN PZN_Verfuegbarkeit pv
                            ON pv.PZN = lp.PZN
                        INNER JOIN WaWiGrossHandelsRabatt gr
                            ON gr.LieferantenID = lp.LieferantenID
                 WHERE lp.Bestellreihenfolge = 1
                 GROUP BY lp.ListeName
            ";

            using (var dt = DatabaseHelper.ExecuteDataTable(cmdTxt))
            {
                foreach (DataRow dr in dt.Rows)
                {
                    DtoBestPrice item = Create(dr);
                    if (item != null)
                    {
                        result.Add(item);
                    }
                }
            }

            return result;
        }
    }
}
