﻿using Aponeo.Basis;
using Aponeo.Business.GH_Konditionen.WholesalerList;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Aponeo.Business.GH_Konditionen.DTOs
{
    public class DtoConditionFileEntry
    {

        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public int PrioId { get; set; }
        public string FileName { get; set; }
        public DateTime LastModified { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime? CheckedAt { get; set; }
        public DateTime? ImportedAt { get; set; }
        public DateTime? LastImportedAt { get; set; }
        public bool IsNew { get; private set; }

        public ConditionComparison ConditionComparison { get; set; }
        public DateTime ImportDate { get; set; }
        public string UrlToChanges { get; set; }

        public DtoConditionFileEntry(int pPrioId, string pFileName, DateTime pLastModified, DateTime pValidFrom, DateTime? pCheckedAt = null, DateTime? pImportedAt = null, DateTime? pLastImportedAt = null, bool pIsNew = true)
        {
            PrioId = pPrioId;
            FileName = pFileName;
            LastModified = pLastModified;
            ValidFrom = pValidFrom;
            CheckedAt = pCheckedAt;
            ImportedAt = pImportedAt;
            LastImportedAt = pLastImportedAt;
            IsNew = pIsNew;
        }

        public static IEnumerable<DtoConditionFileEntry> GetConditionFileEntries(int pPrioId)
        {
            List<DtoConditionFileEntry> result = new List<DtoConditionFileEntry>();

            string cmdTxt = @"
                SELECT [ArtikelbezugKonditionenPriorisierungID]
                      ,[Dateiname]
                      ,[DateiVersion]
                      ,[GueltigAb]
                      ,[Geprueft]
                      ,[Eingespielt]
                      ,[ZuletztEingespielt]
                  FROM [WaWiArtikelbezugKonditionenDateien]
                 WHERE [ArtikelbezugKonditionenPriorisierungID] = @ArtikelbezugKonditionenPriorisierungID
            ";

            List<SqlParameter> sqlParameters = new List<SqlParameter>
            {
                new SqlParameter("@ArtikelbezugKonditionenPriorisierungID", pPrioId),
            };

            using (var dt = DatabaseHelper.ExecuteDataTable(cmdTxt, sqlParameters))
            {
                foreach (DataRow record in dt.Rows)
                {
                    DtoConditionFileEntry item = Create(record);
                    if (item != null)
                    {
                        result.Add(item);
                    }
                }
            }

            return result;
        }

        public static void PrepareLastConditionFileEntriesForReimport()
        {
            List<DtoConditionFileEntry> result = new List<DtoConditionFileEntry>();

            string cmdTxt = @"
                UPDATE a SET ZuletztEingespielt = NULL
                  FROM (SELECT *
                              ,ROW_NUMBER() OVER (PARTITION BY ArtikelbezugKonditionenPriorisierungID
                                                      ORDER BY d.GueltigAb DESC) AS rn
                          FROM [WaWiArtikelbezugKonditionenDateien] d) a
                 WHERE a.rn = 1
            ";

            DatabaseHelper.ExecuteNonQuery(cmdTxt);
        }

        private static DtoConditionFileEntry Create(DataRow pDataRow)
        {
            try
            {
                return new DtoConditionFileEntry(pDataRow.GetValueOrDefault<int>("ArtikelbezugKonditionenPriorisierungID"),
                                                 pDataRow.GetValueOrDefault<string>("Dateiname"),
                                                 pDataRow.GetValueOrDefault<DateTime>("DateiVersion"),
                                                 pDataRow.GetValueOrDefault<DateTime>("GueltigAb"),
                                                 pDataRow.GetValueOrDefault<DateTime?>("Geprueft"),
                                                 pDataRow.GetValueOrDefault<DateTime?>("Eingespielt"),
                                                 pDataRow.GetValueOrDefault<DateTime?>("ZuletztEingespielt"),
                                                 false
                                                 );
            }
            catch (Exception e)
            {
                _log.Error($"could not parse {typeof(DtoConditionFileEntry).Name}", e);
                return null;
            }
        }

        public void InsertIntoDatabase(bool pOverrideNulls = false)
        {
            string cmdTxt = @"
                INSERT INTO [WaWiArtikelbezugKonditionenDateien] (ArtikelbezugKonditionenPriorisierungID, Dateiname, DateiVersion, GueltigAb, Geprueft, Eingespielt, ZuletztEingespielt)
                VALUES (@ArtikelbezugKonditionenPriorisierungID, @Dateiname, @DateiVersion, @GueltigAb, @Geprueft, @Eingespielt, @Eingespielt)
            ";

            List<SqlParameter> sqlParameters = new List<SqlParameter>
            {
                new SqlParameter("@ArtikelbezugKonditionenPriorisierungID", PrioId),
                new SqlParameter("@Dateiname", FileName),
                new SqlParameter("@DateiVersion", LastModified),
                new SqlParameter("@GueltigAb", ValidFrom),
                new SqlParameter("@Geprueft", CheckedAt ?? (pOverrideNulls ? ValidFrom : CheckedAt)),
                new SqlParameter("@Eingespielt", ImportedAt ?? (pOverrideNulls ? ValidFrom : ImportedAt)),
            };

            DatabaseHelper.ExecuteNonQuery(cmdTxt, sqlParameters);
        }

        public void UpdateInDatabase()
        {
            string cmdTxt = @"
                UPDATE [WaWiArtikelbezugKonditionenDateien] 
                   SET Geprueft = @Geprueft
                      ,Eingespielt = ISNULL(Eingespielt, @Eingespielt)
                      ,ZuletztEingespielt = @ZuletztEingespielt
                      ,DateiVersion = @DateiVersion
                 WHERE ArtikelbezugKonditionenPriorisierungID = @ArtikelbezugKonditionenPriorisierungID
                   AND Dateiname = @Dateiname
            ";

            List<SqlParameter> sqlParameters = new List<SqlParameter>
            {
                new SqlParameter("@ArtikelbezugKonditionenPriorisierungID", PrioId),
                new SqlParameter("@Dateiname", FileName),
                new SqlParameter("@Geprueft", CheckedAt),
                new SqlParameter("@Eingespielt", ImportedAt),
                new SqlParameter("@ZuletztEingespielt", LastImportedAt),
                new SqlParameter("@DateiVersion", LastModified),
            };

            DatabaseHelper.ExecuteNonQuery(cmdTxt, sqlParameters);
        }

        /// <summary>
        /// checks whether the file has to imported right now
        /// </summary>
        /// <returns></returns>
        public bool HasToBeImported()
        {
            return WholesalerListImporterBase.HasToBeImported(this);
        }

    }
}
