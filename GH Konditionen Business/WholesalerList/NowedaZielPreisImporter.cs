﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using Aponeo.Business.GH_Konditionen.Configuration;
using Aponeo.Business.GH_Konditionen.DTOs;
using log4net;

namespace Aponeo.Business.GH_Konditionen.WholesalerList
{
    public class NowedaZielPreisImporter : WholesalerListImporterBase
    {
        protected override ILog _log => LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public NowedaZielPreisImporter(ImporterConfiguration pConfig) : base(pConfig)
        {
        }

        public override string SharePointSubfolder => "Noweda Ziel-Preise";

        protected override int WholesalerConditionsPriorityListID => 30;

        protected override string FilenamePattern => $"Noweda Ziel-Preise {Constants.DATE_REGEX}.xlsx";

        public override List<DtoCondition> ParseConditionsToImport(DtoConditionFileEntry pConditionFileEntry, StreamReader pStreamReader)
        {
            // die Liste ist nicht mehr in Benutzung seit 01.07.2024
            throw new Exception("deprecated");

            List<DtoCondition> result = new List<DtoCondition>();

            DataTable dt = ExcelToDataTable(pStreamReader.BaseStream, true, new (string Name, Type DataType)[] {
                ("PZN", typeof(int)),
                ("Discount", typeof(decimal)),
            });

            result = ParseRows(dt);

            return result;
        }

        private List<DtoCondition> ParseRows(DataTable dt)
        {
            List<DtoCondition> result = new List<DtoCondition>();

            int rn = 0;
            try
            {
                foreach (DataRow dr in dt.Rows)
                {
                    rn++;
                    if (int.TryParse(dr["PZN"].ToString(), out int pzn))
                    {
                        decimal discount = decimal.Parse(dr["Discount"].ToString());
                        result.Add(new DtoCondition(WholesalerConditionsPriorityListID, pzn, null, discount));
                    }
                    else
                    {
                        _log.Info($"row number {rn} has empty PZN");
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Warn($"error when parsing row number {rn}", ex);
                throw ex;
            }

            return result;
        }

    }
}
