﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using Aponeo.Business.GH_Konditionen.Configuration;
using Aponeo.Business.GH_Konditionen.DTOs;
using log4net;

namespace Aponeo.Business.GH_Konditionen.WholesalerList
{
    public class GeheBasicImporter : WholesalerListImporterBase
    {
        protected override ILog _log => LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public GeheBasicImporter(ImporterConfiguration pConfig) : base(pConfig)
        {
        }

        public override string SharePointSubfolder => "GEHE Basic";

        protected override int WholesalerConditionsPriorityListID => 5;

        protected override string FilenamePattern => $"GEHE_Basic {Constants.DATE_REGEX}.xlsx";

        public override List<DtoCondition> ParseConditionsToImport(DtoConditionFileEntry pConditionFileEntry, StreamReader pStreamReader)
        {
            List<DtoCondition> result = new List<DtoCondition>();

            DataTable dt = ExcelToDataTable(pStreamReader.BaseStream, true, new (string Name, Type DataType)[] { 
                ("PZN ID", typeof(int)),
                ("PZN DESC", typeof(string)),
                ("Lieferant", typeof(string)),
                ("Vertriebsstatus", typeof(string)),
                ("Preis AEP", typeof(decimal)),
                ("Basic M1", typeof(int)),
                ("Basic P1", typeof(decimal)),
                ("Basic M2", typeof(int)),
                ("Basic P2", typeof(decimal)),
                ("Basic M3", typeof(int)),
                ("Basic P3", typeof(decimal)),
                ("Basic M4", typeof(int)),
                ("Basic P4", typeof(decimal)) });

            result = ParseRows(dt);

            return result;
        }

        private List<DtoCondition> ParseRows(DataTable dt)
        {
            List<DtoCondition> result = new List<DtoCondition>();

            foreach (DataRow dr in dt.Rows)
            {
                if (int.TryParse(dr["PZN ID"].ToString(), out int pzn))
                {
                    int items = int.Parse(dr["Basic M1"].ToString());
                    decimal price = decimal.Parse(dr["Basic P1"].ToString());

                    if (items == 1)
                    {
                        if (price == 0)
                        {
                            _log.Info($"PZN {pzn}: ignore due to price 0€");
                        }
                        else
                        {
                            price *= Config.GeheBasicDiscount;
                            result.Add(new DtoCondition(WholesalerConditionsPriorityListID, pzn, price));
                        }
                    }
                    else
                    {
                        _log.Warn($"PZN {pzn}: basic amount <> 1");
                    }
                }
            }

            return result;
        }
    }
}
