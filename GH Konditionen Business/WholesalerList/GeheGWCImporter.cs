﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using Aponeo.Business.GH_Konditionen.Configuration;
using Aponeo.Business.GH_Konditionen.DTOs;
using log4net;

namespace Aponeo.Business.GH_Konditionen.WholesalerList
{
    public class GeheGWCImporter : WholesalerListImporterBase
    {
        protected override ILog _log => LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public GeheGWCImporter(ImporterConfiguration pConfig) : base(pConfig)
        {
        }

        public override string SharePointSubfolder => "GEHE GWC";

        protected override int WholesalerConditionsPriorityListID => 4;

        protected override string FilenamePattern => $"GEHE_GWC {Constants.DATE_REGEX}.xlsx";

        public override List<DtoCondition> ParseConditionsToImport(DtoConditionFileEntry pConditionFileEntry, StreamReader pStreamReader)
        {
            List<DtoCondition> result = new List<DtoCondition>();

            DataTable dt = ExcelToDataTable(pStreamReader.BaseStream, true, new (string Name, Type DataType)[] {
                ("PZN", typeof(int)),
                ("Artikel", typeof(string)),
                ("Hersteller", typeof(string)),
                ("Preis AEP", typeof(decimal)),
                ("1. Staffel [Menge]", typeof(int)),
                ("1. Staffel [Preis]", typeof(decimal)),
                ("2. Staffel [Menge]", typeof(int)),
                ("2. Staffel [Preis]", typeof(decimal)),
                ("3. Staffel [Menge]", typeof(int)),
                ("3. Staffel [Preis]", typeof(decimal)),
                ("4. Staffel [Menge]", typeof(int)),
                ("4. Staffel [Preis]", typeof(decimal)),
            });

            result = ParseRows(dt);

            return result;
        }

        private List<DtoCondition> ParseRows(DataTable dt)
        {
            List<DtoCondition> result = new List<DtoCondition>();

            int rn = 0;
            try
            {
                foreach (DataRow dr in dt.Rows)
                {
                    rn++;
                    if (int.TryParse(dr["PZN"].ToString(), out int pzn))
                    {
                        int amount = int.Parse(dr["1. Staffel [Menge]"].ToString());
                        decimal price = decimal.Parse(dr["1. Staffel [Preis]"].ToString());

                        // ignore 0€ prices
                        if (amount > 1)
                        {
                            _log.Info($"PZN {pzn}: ignore due to amount '{amount}'");
                        }
                        else if (price == 0)
                        {
                            _log.Info($"PZN {pzn}: ignore due to price 0€");
                        }
                        else
                        {
                            price *= Config.GeheGwcDiscount;
                            result.Add(new DtoCondition(WholesalerConditionsPriorityListID, pzn, price));
                        }
                    }
                    else
                    {
                        _log.Info($"row number {rn} has empty PZN");
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Warn($"error when parsing row number {rn}", ex);
                throw ex;
            }

            return result;
        }
    }
}
