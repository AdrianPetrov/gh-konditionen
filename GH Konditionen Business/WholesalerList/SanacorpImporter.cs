﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using Aponeo.Business.GH_Konditionen.Configuration;
using Aponeo.Business.GH_Konditionen.DTOs;
using log4net;

namespace Aponeo.Business.GH_Konditionen.WholesalerList
{
    public class SanacorpImporter : WholesalerListImporterBase
    {
        protected override ILog _log => LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public SanacorpImporter(ImporterConfiguration pConfig) : base(pConfig)
        {
        }

        public override string SharePointSubfolder => "Sanacorp Minderspanne";

        protected override int WholesalerConditionsPriorityListID => 9;

        protected override string FilenamePattern => $"Sanacorp_Minderspanne {Constants.DATE_REGEX}.xlsx";

        public override List<DtoCondition> ParseConditionsToImport(DtoConditionFileEntry pConditionFileEntry, StreamReader pStreamReader)
        {
            List<DtoCondition> result = new List<DtoCondition>();

            DataTable dt = ExcelToDataTable(pStreamReader.BaseStream, true, new (string Name, Type DataType)[] {
                ("ART_ID", typeof(int)),
                ("PZN", typeof(int)),
                ("ART_NAME", typeof(string)),
                ("EINH_KURZ", typeof(string)),
                ("DAR", typeof(string)),
                ("STAMM_LF_INT_NR", typeof(int)),
                ("LIEF_NAME", typeof(string)),
                ("AEP", typeof(decimal)),
                ("AKTION_ID", typeof(int)),
                ("MENGE_AB", typeof(int)),
                ("STAFFEL_PREIS", typeof(decimal)),
            });

            result = ParseRows(dt);

            return result;
        }

        private List<DtoCondition> ParseRows(DataTable dt)
        {
            List<DtoCondition> result = new List<DtoCondition>();

            foreach (DataRow dr in dt.Rows)
            {
                if (int.TryParse(dr["PZN"].ToString(), out int pzn))
                {
                    int amount = int.Parse(dr["MENGE_AB"].ToString());
                    decimal price = decimal.Parse(dr["STAFFEL_PREIS"].ToString());

                    if (amount > 1)
                    {
                        _log.Info($"PZN {pzn}: ignore due to amount '{amount}'");
                    }
                    else if (price == 0)
                    {
                        _log.Info($"PZN {pzn}: ignore due to price 0€");
                    }
                    else
                    {
                        price *= Config.SanacorpDiscount;
                        result.Add(new DtoCondition(WholesalerConditionsPriorityListID, pzn, price));
                    }
                }
            }

            return result;
        }
    }
}
