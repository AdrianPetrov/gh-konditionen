﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Aponeo.Business.GH_Konditionen.Configuration;
using Aponeo.Business.GH_Konditionen.DTOs;
using log4net;

namespace Aponeo.Business.GH_Konditionen.WholesalerList
{
    public class NowedaKontingentTaxeEkImporter : WholesalerListImporterBase
    {
        protected override ILog _log => LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public NowedaKontingentTaxeEkImporter(ImporterConfiguration pConfig) : base(pConfig)
        {
        }

        public override string SharePointSubfolder => "Noweda Kontingent TaxeEk";

        protected override int WholesalerConditionsPriorityListID => 29;

        protected override string FilenamePattern => $"KontingentListeTaxeEk_{Constants.DATE_REGEX}.csv";

        private const string LINE_PATTERN = "^[0-9]{1,8}$";
        private const int DISCOUNT = 0;

        public override List<DtoCondition> ParseConditionsToImport(DtoConditionFileEntry pConditionFileEntry, StreamReader pStreamReader)
        {
            List<DtoCondition> result = new List<DtoCondition>();

            int lineNr = 0;
            string line = String.Empty;
            while ((line = pStreamReader.ReadLine()) != null)
            {
                lineNr++;
                if (String.IsNullOrEmpty(line))
                {
                    throw new NotSupportedException($"unexpected empty line at line number {lineNr}");
                }
                else
                {
                    // line in the middle
                    DtoCondition condition = ParseConditionLine(line, lineNr);
                    if (condition != null)
                    {
                        result.Add(condition);
                    }
                }
            }

            return result;
        }

        private DtoCondition ParseConditionLine(string pLine, int pLineNr)
        {
            DtoCondition result = null;

            Match match = Regex.Match(pLine, LINE_PATTERN);
            if (!match.Success)
            {
                throw new NotSupportedException($"unexpected line format '{pLine}'");
            }

            int pzn = int.Parse(pLine);

            if (pzn == 0)
            {
                _log.Info($"unknown PZN at line nr {pLineNr}");
            }
            else
            {
                result = new DtoCondition(WholesalerConditionsPriorityListID, pzn, null, DISCOUNT);
            }

            return result;
        }
    }
}
