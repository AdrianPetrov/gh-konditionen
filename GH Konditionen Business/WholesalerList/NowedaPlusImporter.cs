﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Aponeo.Business.GH_Konditionen.Configuration;
using Aponeo.Business.GH_Konditionen.DTOs;
using log4net;

namespace Aponeo.Business.GH_Konditionen.WholesalerList
{
    public class NowedaPlusImporter : WholesalerListImporterBase
    {
        protected override ILog _log => LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public NowedaPlusImporter(ImporterConfiguration pConfig) : base(pConfig)
        {
        }

        public override string SharePointSubfolder => "Noweda PLUS";

        protected override int WholesalerConditionsPriorityListID => 25;

        protected override string FilenamePattern => $"20PLUS_{Constants.DATE_REGEX}.NOW";

        private const string LINE_PATTERN = "^2[0-9]{8}.{40}[0-9]{58}$";

        public override List<DtoCondition> ParseConditionsToImport(DtoConditionFileEntry pConditionFileEntry, StreamReader pStreamReader)
        {
            List<DtoCondition> result = new List<DtoCondition>();

            List<int> discountExceptionList = new List<int>(); //Config.NowedaDiscountExceptionList.Split(',').Select(s => int.Parse(s)).ToList();

            int lineNr = 0;
            string line = String.Empty;
            while ((line = pStreamReader.ReadLine()) != null)
            {
                lineNr++;
                if (String.IsNullOrEmpty(line))
                {
                    throw new NotSupportedException($"unexpected empty line at line number {lineNr}");
                }
                else if (pStreamReader.EndOfStream)
                {
                    // last line
                    ParseLastLine(line, lineNr);
                }
                else if (lineNr == 1)
                {
                    // first line
                    ParseFirstLine(line, pConditionFileEntry.ValidFrom);
                }
                else
                {
                    // line in the middle
                    DtoCondition condition = ParseConditionLine(line, lineNr, discountExceptionList);
                    if (condition != null)
                    {
                        result.Add(condition);
                    }
                }
            }

            return result;
        }

        private DtoCondition ParseConditionLine(string pLine, int pLineNr, List<int> pDiscountExceptionList)
        {
            DtoCondition result = null;

            Match match = Regex.Match(pLine, LINE_PATTERN);
            if (!match.Success)
            {
                throw new NotSupportedException($"unexpected line format '{pLine}'");
            }

            int pzn = int.Parse(pLine.Substring(1, 8));
            decimal priceInCent = int.Parse(pLine.Substring(64, 6));
            int amount = int.Parse(pLine.Substring(55, 9));

            if (pzn == 0)
            {
                _log.Info($"unknown PZN at line nr {pLineNr}");
            }
            else if (amount > 1)
            {
                _log.Info($"PZN {pzn} ignore entry with amount {amount}");
            }
            else if (priceInCent == 0)
            {
                _log.Info($"PZN {pzn} ignore entry with price 0€");
            }
            else
            {
                if (!pDiscountExceptionList.Contains(pzn))
                {
                    priceInCent *= Config.NowedaDiscount;
                }                
                result = new DtoCondition(WholesalerConditionsPriorityListID, pzn, priceInCent / 100.0m);
            }

            return result;
        }

        private void ParseFirstLine(string pLine, DateTime pValidFrom)
        {
            // validity range
            string fileCreationDateTxt = pLine.Substring(8, 6);
            string validityDateTxtFrom = pLine.Substring(14, 6);
            string validityDateTxtTo = pLine.Substring(20, 6);

            DateTime fileCreationDate = DateTime.ParseExact(fileCreationDateTxt, "ddMMyy", CultureInfo.InvariantCulture);
            DateTime validityFrom = DateTime.ParseExact(validityDateTxtFrom, "ddMMyy", CultureInfo.InvariantCulture);
            DateTime validityTo = DateTime.ParseExact(validityDateTxtTo, "ddMMyy", CultureInfo.InvariantCulture);

            if (!pLine.StartsWith("NOWEDA") ||
                pLine.Substring(26, 17) != "NOWEDA20+-ANGEBOT")
            {
                throw new NotSupportedException($"unexpected header '{pLine}'");
            }
            if (validityFrom != pValidFrom)
            {
                throw new NotSupportedException($"unexpected validity date in file header '{validityDateTxtFrom}', expected date {pValidFrom.ToString("ddMMyy")}");
            }
            if (validityTo <= validityFrom)
            {
                throw new NotSupportedException($"unexpected validity date range in file header: '{pLine}'");
            }
            if (fileCreationDate > DateTime.Now)
            {
                throw new NotSupportedException($"unexpected file creation date in file header: '{pLine}'");
            }
        }

        private void ParseLastLine(string pLine, int pLineNr)
        {
            if (!pLine.StartsWith("NOWEDA") ||
                pLine.Substring(26, 17) != "NOWEDA20+-ANGEBOT")
            {
                throw new NotSupportedException($"unexpected footer '{pLine}'");
            }
            int nrFileEntries = int.Parse(pLine.Substring(8, 12)) + 2;
            if (nrFileEntries != pLineNr)
            {
                throw new NotSupportedException($"unexpected number of file entries (found {nrFileEntries}, expected {pLineNr})");
            }
        }
    }
}
