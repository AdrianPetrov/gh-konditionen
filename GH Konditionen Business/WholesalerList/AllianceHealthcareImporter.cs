﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using Aponeo.Business.GH_Konditionen.Configuration;
using Aponeo.Business.GH_Konditionen.DTOs;
using log4net;

namespace Aponeo.Business.GH_Konditionen.WholesalerList
{
    public class AllianceHealthcareImporter : WholesalerListImporterBase
    {
        protected override ILog _log => LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public override string SharePointSubfolder => "Alliance Healthcare";

        protected override int WholesalerConditionsPriorityListID => 20;

        protected override string FilenamePattern => $"ahd {Constants.DATE_REGEX}.xlsx";
        
        public AllianceHealthcareImporter(ImporterConfiguration pConfig) : base(pConfig)
        {
        }

        public override List<DtoCondition> ParseConditionsToImport(DtoConditionFileEntry pConditionFileEntry, StreamReader pStreamReader)
        {
            List<DtoCondition> result = new List<DtoCondition>();

            DataTable dt = ExcelToDataTable(pStreamReader.BaseStream, true, new (string Name, Type DataType)[] {
                ("Material", typeof(int)),
                ("Artikelbezeichnung", typeof(string)),
                ("Packungsgröße", typeof(string)),
                ("Hersteller", typeof(string)),
                ("KZ_KALK", typeof(string)),
                ("EAN/UPC", typeof(string)),
                ("MWST-Satz", typeof(string)),
                ("AEP", typeof(decimal)),
                ("SAEP", typeof(decimal))
            });

            result = ParseRows(dt);

            return result;
        }

        private List<DtoCondition> ParseRows(DataTable dt)
        {
            List<DtoCondition> result = new List<DtoCondition>();

            foreach (DataRow dr in dt.Rows)
            {
                if (int.TryParse(dr["Material"].ToString(), out int pzn))
                {
                    decimal price = decimal.Parse(dr["SAEP"].ToString());

                    // ignore 0€ prices
                    if (price == 0)
                    {
                        _log.Info($"PZN {pzn}: ignore due to price 0€");
                    }
                    else
                    {
                        result.Add(new DtoCondition(WholesalerConditionsPriorityListID, pzn, price));
                    }
                }
            }

            return result;
        }

    }
}
