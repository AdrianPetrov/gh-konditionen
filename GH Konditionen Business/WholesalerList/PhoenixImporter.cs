﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Aponeo.Basis;
using Aponeo.Business.GH_Konditionen.Configuration;
using Aponeo.Business.GH_Konditionen.DTOs;
using log4net;

namespace Aponeo.Business.GH_Konditionen.WholesalerList
{
    public class PhoenixImporter : WholesalerListImporterBase
    {
        protected override ILog _log => LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public PhoenixImporter(ImporterConfiguration pConfig) : base(pConfig)
        {
        }

        public override string SharePointSubfolder => "Phoenix Minderspanne";

        protected override int WholesalerConditionsPriorityListID => 1;

        protected override string FilenamePattern => $"Phoenix_Minderspanne {Constants.DATE_REGEX}.xlsx";

        public override List<DtoCondition> ParseConditionsToImport(DtoConditionFileEntry pConditionFileEntry, StreamReader pStreamReader)
        {
            List<DtoCondition> result = new List<DtoCondition>();

            DataTable dt = ExcelToDataTable(pStreamReader.BaseStream, true, new(string Name, Type DataType)[] {
                ("PZN", typeof(int)),
                ("ArticleName", typeof(string))
            });

            List<DtoTaxeEk> taxeEks = DtoTaxeEk.GetTaxeEks().ToList();
            decimal discount = GetMinderspannenRabatt();
            result = ParseRows(dt, discount, taxeEks);

            return result;
        }

        private List<DtoCondition> ParseRows(DataTable dt, decimal pDiscount, List<DtoTaxeEk> pTaxeEks)
        {
            List<DtoCondition> result = new List<DtoCondition>();

            Dictionary<int, DtoTaxeEk> taxeEkDict = pTaxeEks.ToDictionary(e => e.Pzn, e => e);

            foreach (DataRow dr in dt.Rows)
            {
                if (int.TryParse(dr["PZN"].ToString(), out int pzn))
                {
                    if (!taxeEkDict.ContainsKey(pzn))
                    {
                        _log.Info($"PZN {pzn}: ignore due to no TaxeEk known");
                    }
                    else
                    {
                        decimal taxeEk = taxeEkDict[pzn].TaxePurchasePrice;
                        if (taxeEk == 0)
                        {
                            _log.Info($"PZN {pzn}: ignore due to TaxeEk = 0€");
                        }
                        else
                        {
                            decimal price = taxeEk * (100 - pDiscount) / 100.0m;
                            result.Add(new DtoCondition(WholesalerConditionsPriorityListID, pzn, price));
                        }
                    }
                }
            }

            return result;
        }

        private decimal GetMinderspannenRabatt()
        {
            string cmdTxt = @"
                SELECT MinderspannRabatt
                  FROM WaWiGrossHandelsRabatt ghr
                 WHERE ghr.LieferantenID = 1";

            decimal discount = DatabaseHelper.ExecuteScalar<decimal>(cmdTxt);

            if (discount <= 0 || discount >= 100)
            {
                throw new NotSupportedException($"unreasonable discount {discount}");
            }

            return discount;
        }
    }
}
