﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Aponeo.Basis;
using Aponeo.Business.GH_Konditionen.Configuration;
using Aponeo.Business.GH_Konditionen.DTOs;
using log4net;

namespace Aponeo.Business.GH_Konditionen.WholesalerList
{
    public class GeheServiceArtikel100Importer : WholesalerListImporterBase
    {
        protected override ILog _log => LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public GeheServiceArtikel100Importer(ImporterConfiguration pConfig) : base(pConfig)
        {
        }

        public override string SharePointSubfolder => "GEHE Serviceartikel";

        protected override int WholesalerConditionsPriorityListID => 6;

        protected override string FilenamePattern => $"GEHE_Serviceartikel_100_{Constants.DATE_REGEX}.xlsx";

        public override List<DtoCondition> ParseConditionsToImport(DtoConditionFileEntry pConditionFileEntry, StreamReader pStreamReader)
        {
            List<DtoCondition> result = new List<DtoCondition>();

            // there are four data columns, but the header has two combind columns
            DataTable dt = ExcelToDataTable(pStreamReader.BaseStream, true, new (string Name, Type DataType)[] {
                ("Artikel", typeof(int)),
                ("", typeof(string)),
                ("Artikelgruppe", typeof(string)),
                ("", typeof(string)),
            },
            pExtraLinesRowsToSkip: 2
            );

            List<DtoTaxeEk> taxeEks = DtoTaxeEk.GetTaxeEks().ToList();
            decimal discount = GetMinderspannenRabatt();
            result = ParseRows(dt, discount, taxeEks);

            return result;
        }

        private List<DtoCondition> ParseRows(DataTable dt, decimal pDiscount, List<DtoTaxeEk> pTaxeEks)
        {
            List<DtoCondition> result = new List<DtoCondition>();

            Dictionary<int, DtoTaxeEk> taxeEkDict = pTaxeEks.ToDictionary(e => e.Pzn, e => e);

            foreach (DataRow dr in dt.Rows)
            {
                if (int.TryParse(dr["Artikel"].ToString(), out int pzn))
                {
                    if (!taxeEkDict.ContainsKey(pzn))
                    {
                        _log.Info($"PZN {pzn}: ignore due to no TaxeEk known");
                    }
                    else
                    {
                        decimal taxeEk = taxeEkDict[pzn].TaxePurchasePrice;
                        if (taxeEk == 0)
                        {
                            _log.Info($"PZN {pzn}: ignore due to TaxeEk = 0€");
                        }
                        else
                        {
                            decimal price = taxeEk * (100 - pDiscount) / 100.0m;
                            result.Add(new DtoCondition(WholesalerConditionsPriorityListID, pzn, price));
                        }
                    }
                }
            }

            return result;
        }

        private decimal GetMinderspannenRabatt()
        {
            string cmdTxt = @"
                SELECT MinderspannRabatt
                  FROM WaWiGrossHandelsRabatt ghr
                 WHERE ghr.LieferantenID = 2";

            decimal discount = DatabaseHelper.ExecuteScalar<decimal>(cmdTxt);

            if (discount <= 0 || discount >= 100)
            {
                throw new NotSupportedException($"unreasonable discount {discount}");
            }

            return discount;
        }
    }
}
