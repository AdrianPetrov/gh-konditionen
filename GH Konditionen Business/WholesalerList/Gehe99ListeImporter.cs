﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Aponeo.Business.GH_Konditionen.Configuration;
using Aponeo.Business.GH_Konditionen.DTOs;
using log4net;

namespace Aponeo.Business.GH_Konditionen.WholesalerList
{
    public class Gehe99ListeImporter : WholesalerListImporterBase
    {
        protected override ILog _log => LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public Gehe99ListeImporter(ImporterConfiguration pConfig) : base(pConfig)
        {
        }

        public override string SharePointSubfolder => "Gehe 99-Liste";

        protected override int WholesalerConditionsPriorityListID => 28;

        protected override string FilenamePattern => $"GEHE_99-Liste {Constants.DATE_REGEX}.xlsx";

        public override List<DtoCondition> ParseConditionsToImport(DtoConditionFileEntry pConditionFileEntry, StreamReader pStreamReader)
        {
            List<DtoCondition> result = new List<DtoCondition>();

            DataTable dt = ExcelToDataTable(pStreamReader.BaseStream, true, new (string Name, Type DataType)[] {
                ("PZN", typeof(int)),
                ("Anzeigename", typeof(string)),
                ("Hersteller", typeof(string)),
                ("Rabatt Lauer", typeof(decimal)),
                ("Lauer Preis", typeof(decimal)),
            });

            List<DtoTaxeEk> taxeEks = DtoTaxeEk.GetTaxeEks().ToList();
            result = ParseRows(dt, taxeEks);

            return result;
        }

        private List<DtoCondition> ParseRows(DataTable dt, List<DtoTaxeEk> pTaxeEks)
        {
            List<DtoCondition> result = new List<DtoCondition>();

            Dictionary<int, DtoTaxeEk> taxeEkDict = pTaxeEks.ToDictionary(e => e.Pzn, e => e);

            foreach (DataRow dr in dt.Rows)
            {
                int pzn = int.Parse(dr["PZN"].ToString());

                if (!taxeEkDict.ContainsKey(pzn))
                {
                    _log.Info($"PZN {pzn}: ignore due to no TaxeEk known");
                }
                else
                {
                    decimal taxeEk = taxeEkDict[pzn].TaxePurchasePrice;
                    if (taxeEk == 0)
                    {
                        _log.Info($"PZN {pzn}: ignore due to TaxeEk = 0€");
                    }
                    else
                    {
                        decimal discount = decimal.Parse(dr["Rabatt Lauer"].ToString());
                        decimal price = taxeEk * (100 - discount) / 100.0m;
                        result.Add(new DtoCondition(WholesalerConditionsPriorityListID, pzn, price));
                    }
                }
            }

            return result;
        }

    }
}
