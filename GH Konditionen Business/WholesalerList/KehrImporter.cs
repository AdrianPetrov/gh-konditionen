﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using Aponeo.Business.GH_Konditionen.Configuration;
using Aponeo.Business.GH_Konditionen.DTOs;
using log4net;

namespace Aponeo.Business.GH_Konditionen.WholesalerList
{
    public class KehrImporter : WholesalerListImporterBase
    {
        protected override ILog _log => LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public override string SharePointSubfolder => "KEHR";

        protected override int WholesalerConditionsPriorityListID => 17;

        protected override string FilenamePattern => $"KEHR PP_WAVE_basic {Constants.DATE_REGEX}.dat";

        private const string LINE_PATTERN1 = "^01 [0-9]+ [0-9]+ [0-9]+ [0-9]{3} [0-9]{7} [0-9]{3} [0-9]{7} [0-9]{3} [0-9]$";
        private const string LINE_PATTERN2 = "^01 [0-9]+ [0-9]+ [0-9]+ [0-9]{3} [0-9]{7} [0-9]{3} [0-9]{7} [0-9]{3} [0-9]{7} [0-9]{3} [0-9]{7} [0-9]{3} [0-9]$";

        public KehrImporter(ImporterConfiguration pConfig) : base(pConfig)
        {
        }

        public override List<DtoCondition> ParseConditionsToImport(DtoConditionFileEntry pConditionFileEntry, StreamReader pStreamReader)
        {
            List<DtoCondition> result = new List<DtoCondition>();

            int lineNr = 0;
            string line = String.Empty;
            while ((line = pStreamReader.ReadLine()) != null)
            {
                lineNr++;
                if (String.IsNullOrEmpty(line))
                {
                    throw new NotSupportedException($"unexpected empty line at line number {lineNr}");
                }
                else if (pStreamReader.EndOfStream)
                {
                    // last line
                    ParseLastLine(line, lineNr);
                }
                else if (lineNr == 1)
                {
                    // first line
                    ParseFirstLine(line, pConditionFileEntry.ValidFrom);
                }
                else
                {
                    // line in the middle
                    DtoCondition condition = ParseConditionLine(line, lineNr);
                    if (condition != null)
                    {
                        result.Add(condition);
                    }
                }
            }

            return result;
        }

        private DtoCondition ParseConditionLine(string pLine, int pLineNr)
        {
            DtoCondition result = null;

            Match match1 = Regex.Match(pLine, LINE_PATTERN1);
            Match match2 = Regex.Match(pLine, LINE_PATTERN2);
            if (!match1.Success && !match2.Success)
            {
                throw new NotSupportedException($"unexpected line format '{pLine}'");
            }

            int pzn = int.Parse(pLine.Substring(3, 8));
            decimal priceInCent = int.Parse(pLine.Substring(20, 8));
            int minNumberOfPurchases = int.Parse(pLine.Substring(28, 3));

            if (minNumberOfPurchases == 1)
            {
                priceInCent *= Config.KehrDiscount;
                result = new DtoCondition(WholesalerConditionsPriorityListID, pzn, priceInCent / 100.0m);
            }
            else
            {
                _log.Info($"PZN {pzn} at line {pLineNr} skipped due to min number of purchases = {minNumberOfPurchases}");
            }

            return result;
        }

        private void ParseFirstLine(string pLine, DateTime pValidFrom)
        {
            // constants
            if (pLine.Substring(0, 2) != "00" ||
                pLine.Substring(3, 2) != "01" ||
                pLine.Substring(20, 24) != "PHARMA PRIVAT WAVE basic")
            {
                throw new NotSupportedException($"unexpected header '{pLine}'");
            }

            // validity range
            string validityDateTxtFrom = pLine.Substring(6, 6);
            DateTime validityFrom = DateTime.ParseExact(validityDateTxtFrom, "ddMMyy", CultureInfo.InvariantCulture);
            DateTime validityTo = DateTime.ParseExact(pLine.Substring(13, 6), "ddMMyy", CultureInfo.InvariantCulture);
            if (validityFrom != pValidFrom)
            {
                throw new NotSupportedException($"unexpected validity date in file header '{validityDateTxtFrom}', expected date {pValidFrom.ToString("ddMMyy")}");
            }
            if (validityTo <= validityFrom)
            {
                throw new NotSupportedException($"unexpected validity date range in file header: '{pLine}'");
            }
        }

        private void ParseLastLine(string pLine, int pLineNr)
        {
            if (!pLine.StartsWith("99"))
            {
                throw new NotSupportedException($"unexpected footer '{pLine}'");
            }
            if (int.Parse(pLine.Substring(3, pLine.Length-3)) +2 != pLineNr)
            {
                throw new NotSupportedException($"unexpected number of lines in file: '{pLine}'");
            }
        }

    }
}
