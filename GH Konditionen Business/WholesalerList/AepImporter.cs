﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using Aponeo.Business.GH_Konditionen.Configuration;
using Aponeo.Business.GH_Konditionen.DTOs;
using log4net;

namespace Aponeo.Business.GH_Konditionen.WholesalerList
{
    public class AepImporter : WholesalerListImporterBase
    {
        protected override ILog _log => LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public override string SharePointSubfolder => "AEP";

        protected override int WholesalerConditionsPriorityListID => 12;

        protected override string FilenamePattern => $"Preisliste_von_{Constants.DATE_REGEX}.txt";


        private const string LINE_PATTERN = "^01 [0-9]+ [0-9]+ [0-9]+ 001 [0-9]{7} [0-9]{3} [0-9]{7} [0-9]{3} 0 0 0 0 0 0 0 0 0 [0-9]$";

        public AepImporter(ImporterConfiguration pConfig) : base(pConfig)
        {
        }

        public override List<DtoCondition> ParseConditionsToImport(DtoConditionFileEntry pConditionFileEntry, StreamReader pStreamReader)
        {
            List<DtoCondition> result = new List<DtoCondition>();

            int lineNr = 0;
            string line = String.Empty;
            while ((line = pStreamReader.ReadLine()) != null)
            {
                lineNr++;
                if (String.IsNullOrEmpty(line))
                {
                    throw new NotSupportedException($"unexpected empty line at line number {lineNr}");
                }
                else if (pStreamReader.EndOfStream)
                {
                    // last line
                    ParseLastLine(line);
                }
                else if (lineNr == 1)
                {
                    // first line
                    ParseFirstLine(line, pConditionFileEntry.ValidFrom);
                }
                else
                {
                    // line in the middle
                    DtoCondition condition = ParseConditionLine(line);
                    result.Add(condition);
                }
            }

            return result;
        }

        private DtoCondition ParseConditionLine(string pLine)
        {
            DtoCondition result;

            Match match = Regex.Match(pLine, LINE_PATTERN);
            if (!match.Success)
            {
                throw new NotSupportedException($"unexpected line format '{pLine}'");
            }

            int pzn = int.Parse(pLine.Substring(3, 8));
            int priceInCent = int.Parse(pLine.Substring(20, 8));

            result = new DtoCondition(WholesalerConditionsPriorityListID, pzn, priceInCent / 100.0m);

            return result;
        }

        private void ParseFirstLine(string pLine, DateTime pValidFrom)
        {
            // constants
            if (pLine.Substring(0, 2) != "00" ||
                pLine.Substring(3, 2) != "01" ||
                pLine.Substring(20, 10) != "AEP-Direkt")
            {
                throw new NotSupportedException($"unexpected header '{pLine}'");
            }

            // validity range
            string validityDateTxtFrom = pLine.Substring(6, 6);
            DateTime validityFrom = DateTime.ParseExact(validityDateTxtFrom, "ddMMyy", CultureInfo.InvariantCulture);
            DateTime validityTo = DateTime.ParseExact(pLine.Substring(13, 6), "ddMMyy", CultureInfo.InvariantCulture);
            if (validityFrom != pValidFrom)
            {
                throw new NotSupportedException($"unexpected validity date in file header '{validityDateTxtFrom}', expected date {pValidFrom.ToString("ddMMyy")}");
            }
            if (validityTo <= validityFrom)
            {
                throw new NotSupportedException($"unexpected validity date range in file header: '{pLine}'");
            }
        }

        private void ParseLastLine(string pLine)
        {
            if (!pLine.StartsWith("99"))
            {
                throw new NotSupportedException($"unexpected footer '{pLine}'");
            }
        }
    }
}
