﻿using SpoFile = Microsoft.SharePoint.Client.File;
using Aponeo.Business.GH_Konditionen.DTOs;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.IO;
using Aponeo.Basis;
using System.Data.SqlClient;
using Aponeo.Business.GH_Konditionen.Configuration;
using OfficeOpenXml;
using Aponeo.Tools.ExtensionMethods;

namespace Aponeo.Business.GH_Konditionen.WholesalerList
{
    public abstract class WholesalerListImporterBase
    {
        protected virtual ILog _log { get; }
        private static readonly ILog _logStatic = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static int _hoursToImportBeforeValid;
        public static int _hourToImportAtSaturday;

        protected SharePointManager SharePointManager { get; private set; }
        protected ImporterConfiguration Config { get; private set; }

        public abstract string SharePointSubfolder { get; }
        protected abstract int WholesalerConditionsPriorityListID { get; }
        protected abstract string FilenamePattern { get; }
        public abstract List<DtoCondition> ParseConditionsToImport(DtoConditionFileEntry pConditionFileEntry, StreamReader pStreamReader);

        protected WholesalerListImporterBase(ImporterConfiguration pConfig)
        {
            Config = pConfig;
            SharePointManager = new SharePointManager(pConfig);
            _hoursToImportBeforeValid = pConfig.HoursToImportBeforeValid;
            _hourToImportAtSaturday = pConfig.HourToImportAtSaturday;
        }

        protected virtual string ExtractValidityDateTxtFromFilename(string pFilename)
        {
            int idxStart = FilenamePattern.IndexOf(Constants.DATE_REGEX);
            string filenameWithoutExtension = Path.GetFileNameWithoutExtension(pFilename);
            int length = filenameWithoutExtension.Substring(idxStart).Length;
            string dateTxt = pFilename.Substring(idxStart, length);
            return dateTxt;
        }

        /// <summary>
        /// Analyses new condition files
        /// <para/>
        /// Returns the last file, which has to be imported (somewhen in future)
        /// </summary>
        public DtoConditionFileEntry Analyse(IEnumerable<DtoConditionFileEntry> pNewFilesToProcess)
        {
            _log.Info("Start analysing");

            DtoConditionFileEntry lastNewConditionFile = null;
            try
            {
                // new files in SharePoint
                lastNewConditionFile = pNewFilesToProcess?.OrderByDescending(f => f.ValidFrom).FirstOrDefault();

                if (lastNewConditionFile != null)
                {
                    // register to be skipped files (usually not the case, only when more than one file at once is to be imported)
                    RegisterSkippedConditionFiles(pNewFilesToProcess.Where(x => x != lastNewConditionFile));

                    // analyse the last one
                    // register in database
                    if (lastNewConditionFile.IsNew)
                    {
                        lastNewConditionFile.InsertIntoDatabase();
                    }

                    _log.Notice($"analyse file '{lastNewConditionFile.FileName}'");

                    List<DtoCondition> conditions;
                    using (StreamReader sr = SharePointManager.GetStreamReader(lastNewConditionFile, SharePointSubfolder))
                    {
                        // parse file (and check correct format)
                        conditions = ParseConditionsToImport(lastNewConditionFile, sr).Where(c => c != null).ToList();
                    }

                    // save changes to file entry
                    lastNewConditionFile.ConditionComparison = AnalyseNewConditions(lastNewConditionFile, conditions);

                    if (lastNewConditionFile.ImportedAt == null)
                    {
                        // first import of this file => create report with changes and upload it to SharePoint
                        ExportChangesToSharePoint(lastNewConditionFile);
                    }

                    // calculate import timestamp
                    lastNewConditionFile.ImportDate = GetImportDate(lastNewConditionFile);

                    // mark as done
                    lastNewConditionFile.CheckedAt = DateTime.Now;
                    lastNewConditionFile.UpdateInDatabase();
                }
                else
                {
                    _log.Info("no file to analyse");
                }
            }
            catch (Exception ex)
            {
                _log.Error("error during analysing occured", ex);
            }

            return lastNewConditionFile;
        }

        private void ExportChangesToSharePoint(DtoConditionFileEntry pConditionFile)
        {
            if (Config.SharePointUpload)
            {
                byte[] reportBytes = pConditionFile.ConditionComparison.CreateExcelWithChanges();

                // direct upload to SharePoint
                string filename = Path.GetFileNameWithoutExtension(pConditionFile.FileName);
                pConditionFile.UrlToChanges = SharePointManager.Upload(SharePointSubfolder, reportBytes, $"Changes_{filename}.xlsx");
            }
            else
            {
                _log.Info("upload to SharePoint is deactivated");
            }
        }

        /// <summary>
        /// Gets all to be processed (check + import if valid) files from SharePoint
        /// </summary>
        public List<DtoConditionFileEntry> GetNewFilesToProcess()
        {
            List<DtoConditionFileEntry> result = new List<DtoConditionFileEntry>();

            _log.Info("check for new conditions");

            try
            {
                // already registered files in database
                List<DtoConditionFileEntry> conditionFileEntries = DtoConditionFileEntry.GetConditionFileEntries(WholesalerConditionsPriorityListID).ToList();

                foreach (SpoFile file in SharePointManager.GetAllAvailableFiles(SharePointSubfolder))
                {
                    DtoConditionFileEntry cfe = conditionFileEntries.SingleOrDefault(c => c.FileName == file.Name);
                    DtoConditionFileEntry latestCfe = conditionFileEntries.OrderByDescending(c => c.ValidFrom).FirstOrDefault();

                    if (cfe == null)
                    {
                        _log.Notice($"new condition file '{file.Name}'");
                        if (!IsValidFilename(file.Name))
                        {
                            _log.Error($"filename '{file.Name}' is not valid");
                        }
                        else
                        {
                            result.Add(new DtoConditionFileEntry(WholesalerConditionsPriorityListID,
                                                                 file.Name,
                                                                 file.TimeLastModified,
                                                                 (DateTime)GetValidityDateFromFilename(file.Name)));
                        }
                    }
                    else if (cfe.LastModified < file.TimeLastModified && (latestCfe == null || file.Name == latestCfe.FileName))
                    {
                        _log.Info($"condition file '{file.Name}' was changed");
                        cfe.LastModified = file.TimeLastModified;
                        result.Add(cfe);
                    }
                    else if (cfe.LastImportedAt != null)
                    {
                        _log.Debug($"condition file '{file.Name}' already imported");
                    }
                    else if (cfe.CheckedAt != null)
                    {
                        if (HasToBeImported(cfe))
                        {
                            _log.Info($"condition file '{file.Name}' has to be imported...");
                            result.Add(cfe);
                        }
                        else
                        {
                            _log.Info($"condition file '{file.Name}' not yet valid...");
                        }
                    }
                    else
                    {
                        _log.Info($"condition file '{file.Name}' known but not yet checked");
                        result.Add(cfe);
                    }
                }
            }
            catch (Exception e)
            {
                _log.Error($"could not get files to import", e);
                result.Clear();
            }

            return result;
        }

        /// <summary>
        /// imports the file
        /// </summary>
        /// <param name="pConditionFileEntry"></param>
        public void Import(DtoConditionFileEntry pConditionFileEntry)
        {
            if (pConditionFileEntry != null && pConditionFileEntry.HasToBeImported())
            {
                _log.Notice($"import '{pConditionFileEntry.FileName}'");

                if (pConditionFileEntry.ConditionComparison != null)
                {
                    // import the file
                    ImportConditions(pConditionFileEntry.ConditionComparison);
                    pConditionFileEntry.LastImportedAt = DateTime.Now;
                    pConditionFileEntry.ImportedAt = pConditionFileEntry.ImportedAt ?? pConditionFileEntry.LastImportedAt;
                    pConditionFileEntry.UpdateInDatabase();
                }
                else
                {
                    _log.Warn("file was not yet analysed");
                }
            }
            else
            {
                _log.Warn("no conditions to import");
            }
        }

        public static bool HasToBeImported(DtoConditionFileEntry pConditionFileEntry)
        {
            return GetImportDate(pConditionFileEntry) < DateTime.Now;
        }

        private static DateTime GetImportDate(DtoConditionFileEntry pConditionFileEntry)
        {
            return GetImportDate(pConditionFileEntry.ValidFrom, _hoursToImportBeforeValid, _hourToImportAtSaturday);
        }

        /// <summary>
        /// If a file gets valid at Sunday or Monday, then the import should be done in both cases on Saturday (!) at a certain time. 
        /// The idea is that the prices should be calculated from Saturday on on the basis of the new conditions, since the first wholesaler order will be done on Monday when the new conditions are valid.
        /// </summary>
        /// <param name="pValidFrom"></param>
        /// <param name="pHoursToImportBeforeValid">General delta</param>
        /// <param name="pHourToImportAtSaturday"></param>
        /// <returns></returns>
        public static DateTime GetImportDate(DateTime pValidFrom, int pHoursToImportBeforeValid, int pHourToImportAtSaturday)
        {
            DateTime result;

            DayOfWeek validDayOfWeek = pValidFrom.DayOfWeek;
            if (validDayOfWeek == DayOfWeek.Monday ||
                validDayOfWeek == DayOfWeek.Sunday)
            {
                int daysToAdd = validDayOfWeek == DayOfWeek.Sunday ? 1 : 2;
                result = pValidFrom.AddDays(-1 * daysToAdd).AddHours(pHourToImportAtSaturday);
            }
            else
            {
                result = pValidFrom.AddHours(-1 * pHoursToImportBeforeValid);
            }

            return result;
        }

        private bool IsValidFilename(string pFilename)
        {
            Match match = Regex.Match(pFilename, FilenamePattern);
            bool isValidDateTxt = GetValidityDateFromFilename(pFilename) != null;
            return match.Success && isValidDateTxt;
        }

        private DateTime? GetValidityDate(string pDateTxt)
        {
            DateTime? result = null;
            try
            {
                result = DateTime.ParseExact(pDateTxt, Constants.FILENAME_DATE_PATTERN, CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                _log.Info($"could not parse datetime '{pDateTxt}' with expected format '{Constants.FILENAME_DATE_PATTERN}'", ex);
            }
            return result;
        }

        private DateTime? GetValidityDateFromFilename(string pFilename)
        {
            DateTime? result;
            string validityDateTxt = ExtractValidityDateTxtFromFilename(pFilename);
            result = GetValidityDate(validityDateTxt);
            return result;
        }

        private ConditionComparison AnalyseNewConditions(DtoConditionFileEntry pConditionFileEntry, List<DtoCondition> pNewConditions)
        {
            _log.Info($"analyze new conditions");

            // load actual conditions
            List<DtoCondition> actualConditions = DtoCondition.GetConditions(WholesalerConditionsPriorityListID).ToList();

            // add some missing informations to conditions
            // TODO: Namen zur Speicher-Optimierung nur für die geänderten Datensätze heranziehen
            pNewConditions = DtoCondition.AddAdditionalInformation(pNewConditions).ToList();
            actualConditions = DtoCondition.AddAdditionalInformation(actualConditions).ToList();

            // compare old vs. new conditions
            ConditionComparison conditionComparison = new ConditionComparison(actualConditions.Clone(), pNewConditions.Clone());

            // log the changes
            _log.Notice($"# actual: {conditionComparison.NumberActual}");
            _log.Notice($"# new: {conditionComparison.NumberNew}");
            _log.Notice($"# changed prices: {conditionComparison.Changes.Count()}");
            _log.Notice($"# new prices: {conditionComparison.NewConditions.Count()}");
            _log.Notice($"# invalid prices: {conditionComparison.DeletedConditions.Count()}");

            foreach (KeyValuePair<DtoCondition, DtoCondition> cc in conditionComparison.Changes)
            {
                DtoCondition actualCondition = cc.Key;
                DtoCondition newCondition = cc.Value;
                _log.Info($"PZN {actualCondition.Pzn}: changed purchase price: {actualCondition.Price:0.00} --> {newCondition.Price:0.00}");
            }

            foreach (DtoCondition c in conditionComparison.NewConditions)
            {
                _log.Info($"PZN {c.Pzn}: new purchase price: {c.Price}");
            }

            foreach (DtoCondition c in conditionComparison.DeletedConditions)
            {
                _log.Info($"PZN {c.Pzn}: invalid");
            }

            return conditionComparison;
        }

        private void ImportConditions(ConditionComparison pConditionComparison)
        {
            // insert new items
            foreach (DtoCondition c in pConditionComparison.NewConditions)
            {
                _log.Info($"PZN {c.Pzn}: insert new price {c.Price}");
                c.Insert();
            }

            // delete invalid items
            foreach (DtoCondition c in pConditionComparison.DeletedConditions)
            {
                _log.Info($"PZN {c.Pzn}: delete");
                c.Delete();
            }

            // update changed items
            foreach (KeyValuePair<DtoCondition, DtoCondition> cc in pConditionComparison.Changes)
            {
                DtoCondition actualCondition = cc.Key;
                DtoCondition newCondition = cc.Value;
                _log.Info($"PZN {actualCondition.Pzn}: update changed price: {actualCondition.Price:0.00} --> {newCondition.Price:0.00}");
                actualCondition.Update(newCondition.Price, newCondition.Discount);
            }
        }

        private void RegisterSkippedConditionFiles(IEnumerable<DtoConditionFileEntry> pFilesToSkip)
        {
            int cntToBeSkippedFiles = pFilesToSkip.Count();

            if (cntToBeSkippedFiles > 0)
            {
                _log.Warn($"register {cntToBeSkippedFiles} skipped condition files of {SharePointSubfolder}");

                foreach (DtoConditionFileEntry cfe in pFilesToSkip)
                {
                    _log.Warn($"---> file {cfe.FileName}");
                    cfe.InsertIntoDatabase(true);
                }
            }
        }

        /// <summary>
        /// MERGE all conditions from WaWiArtikelbezugKonditionenBasis into WaWiArtikelbezugLieferantenPrioritaet / Logistik.ArtikelbezugLieferantPrioritaet
        /// </summary>
        public static void MergeNewConditions()
        {
            _logStatic.Notice("Merge new conditions");

            List<SqlCommand> cmds = new List<SqlCommand>
            {

                // for RX - articles the default wholesaler discounts are used, even if they are not conatined in some list (default case) => add them artificially
                new SqlCommand(
                    @"
                    DELETE FROM WaWiArtikelbezugKonditionenBasis
                     WHERE ArtikelbezugKonditionenPriorisierungID IN (2, 7, 10, 13, 18, 21, 26)"
                ),

                new SqlCommand(
                    @"
                    INSERT INTO WaWiArtikelbezugKonditionenBasis (PZN, ArtikelbezugKonditionenPriorisierungID, Einkaufspreis, Rabatt)
                    SELECT a.PZN
                            ,a.ArtikelbezugKonditionenPriorisierungID
                            ,a.TaxeEK * (100 - Rabatt) / 100     AS Einkaufspreis
                            ,a.Rabatt
                        FROM (SELECT ai.PZN
                                    ,akp.ID               AS ArtikelbezugKonditionenPriorisierungID
                                    ,ap.Einkaufspreis     AS TaxeEK
                                    -- TODO: Verwendung von WaWiGrossHandelsRabatt.RxRabatt --> es müssen hierzu die richtigen Werte in die Tabelle!
                                    ,CASE
                                        WHEN akp.LieferantenID = 1      THEN 3.13 -- PHOENIX
                                        WHEN akp.LieferantenID = 2      THEN 3.15 -- GEHE
                                        WHEN akp.LieferantenID = 3      THEN 3.00 -- Sanacorp
                                        WHEN akp.LieferantenID = 765    THEN 0.00 -- AEP
                                        WHEN akp.LieferantenID = 811    THEN 0.00 -- Kehr
						                WHEN akp.LieferantenID = 812    THEN 3.14 -- Alliance
                                        WHEN akp.LieferantenID = 856    THEN 0.00 -- Noweda
                                        ELSE 0
                                    END                  AS Rabatt
                                FROM ArtikelstammPreis ap
                                    INNER JOIN ArtikelstammIdentifikation ai
                                        ON ai.Artikelnummer = ap.Artikelnummer
                                    INNER JOIN WaWiArtikelbezugKonditionenPriorisierung akp
                                        ON akp.ID IN (2, 7, 10, 13, 18, 21, 26)
                                    INNER JOIN WaWiGrossHandelsRabatt ghr
                                        ON ghr.LieferantenID = akp.LieferantenID
                                    INNER JOIN ArtikelstammAttributisierung aa
                                        ON aa.Artikelnummer = ap.Artikelnummer
                                WHERE aa.AttributID = 41
                                AND aa.Wert = 1) a;"
                ),

                // TODO: noch benötigt im alten Schema?
                // merge all new conditions
                new SqlCommand(
                    @"
                    MERGE INTO WaWiArtikelbezugLieferantenPrioritaet t
                    USING ViewWaWiArtikelbezugLieferantenPrioritaet s
                       ON t.PZN = s.PZN
                      AND t.Bestellreihenfolge = s.Bestellreihenfolge
                     WHEN MATCHED AND (t.LieferantenID <> s.LieferantenID OR ISNULL(t.Einkaufspreis, -1) <> ISNULL(s.EK, -1))
                        THEN UPDATE 
                                SET t.LieferantenID = s.LieferantenID
                                   ,t.Einkaufspreis = s.EK
                     WHEN NOT MATCHED BY TARGET
                        THEN INSERT (PZN, Bestellreihenfolge, LieferantenID, Einkaufspreis)
                             VALUES (s.PZN, s.Bestellreihenfolge, s.LieferantenID, s.EK)
                     WHEN NOT MATCHED BY SOURCE
                        THEN DELETE;
                    "
                ),

                // new logistics schema: sync Lieferanten
                new SqlCommand(
                    @"
                    INSERT INTO Logistik.Lieferant (LieferantID, LieferantName, Beschreibung, AnschriftFirmenname, AnschriftStrasse, AnschriftPLZ, AnschriftOrt)
                    SELECT DISTINCT wl.ID, wl.Firmenname, NULL, wl.Firmenname2, wl.Strasse, wl.PLZ, wl.Ort
                      FROM WaWiLieferanten wl 
                            LEFT OUTER JOIN WaWiMsv3 wm 
                                ON wl.ID = wm.LieferantenID
                            LEFT OUTER JOIN Logistik.Lieferant l
                                ON l.LieferantID = wl.ID
                     WHERE l.LieferantID IS NULL
                     ORDER BY wl.ID ASC"
                ),

                // new logistics schema: sync conditions
                new SqlCommand(
                    @"
                    MERGE INTO Logistik.ArtikelbezugLieferantPrioritaet t
                    USING WaWiArtikelbezugLieferantenPrioritaet s
                       ON t.PZN = s.PZN
                      AND t.Bestellreihenfolge = s.Bestellreihenfolge
                     WHEN MATCHED AND (t.LieferantID <> s.LieferantenID OR ISNULL(t.Einkaufspreis, -1) <> ISNULL(s.Einkaufspreis, -1))
                        THEN UPDATE 
                                SET t.LieferantID = s.LieferantenID
                                   ,t.Einkaufspreis = s.Einkaufspreis
                     WHEN NOT MATCHED BY TARGET
                        THEN INSERT (PZN, Bestellreihenfolge, LieferantID, Einkaufspreis)
                             VALUES (s.PZN, s.Bestellreihenfolge, s.LieferantenID, s.Einkaufspreis)
                     WHEN NOT MATCHED BY SOURCE
                        THEN DELETE;
                    "
                ),
            };

            DatabaseHelper.ExecuteNonQuery(cmds, useTransaction: true);
            _logStatic.Notice("...new conditions merged");
        }

        protected DataTable ExcelToDataTable(Stream pStream, bool pHasHeaderRow, (string Name, Type DataType)[] pTableMetaData, int pExtraLinesRowsToSkip = 0)
        {
            DataTable dt;

            using (MemoryStream ms = new MemoryStream())
            {
                pStream.CopyTo(ms);
                using (ExcelPackage excelPackage = new ExcelPackage(ms))
                {
                    ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets[1];

                    //check if the worksheet is completely empty
                    if (worksheet.Dimension == null)
                    {
                        return null;
                    }

                    dt = CreateDataTable(pExtraLinesRowsToSkip, pHasHeaderRow, worksheet, pTableMetaData);

                    AddRowsToDataTable(worksheet, dt, Convert.ToInt32(pHasHeaderRow) + pExtraLinesRowsToSkip);
                }
            }

            return dt;
        }

        private void AddRowsToDataTable(ExcelWorksheet pWorksheet, DataTable pDataTable, int pLinesRowsToSkip)
        {
            for (int r = pWorksheet.Dimension.Start.Row + pLinesRowsToSkip; r <= pWorksheet.Dimension.End.Row; r++)
            {
                // create a new datatable row
                DataRow row = pDataTable.NewRow();

                // loop through all columns
                for (int c = pWorksheet.Dimension.Start.Column; c <= pWorksheet.Dimension.End.Column; c++)
                {
                    var excelCell = pWorksheet.Cells[r, c].Value;

                    // add cell value to the datatable
                    if (excelCell != null)
                    {
                        try
                        {
                            row[c - 1] = excelCell;
                        }
                        catch (Exception ex)
                        {
                            _log.Error($"Row {r - 1}, Column {c}: Invalid {pDataTable.Columns[c - 1].DataType.ToString().Replace("System.", "")} value: {excelCell.ToString()}", ex);
                        }
                    }
                }

                //add the new row to the datatable
                pDataTable.Rows.Add(row);
            }
        }

        private DataTable CreateDataTable(int pExtraLinesRowsToSkip, bool pHasHeaderRow, ExcelWorksheet pWorksheet, (string Name, Type DataType)[] pTableMetaData)
        {
            DataTable dt = new DataTable();

            for (int c = pWorksheet.Dimension.Start.Column; c <= pWorksheet.Dimension.End.Column; c++)
            {
                if (pTableMetaData.Length < c)
                {
                    break;
                }
                string columnName = GetColumnName(pExtraLinesRowsToSkip, pHasHeaderRow, pWorksheet, c);
                string expectedColumnName = pTableMetaData[c - 1].Name;
                if (columnName != expectedColumnName)
                {
                    throw new NotSupportedException($"unexpected column name at position {c}: expected = '{expectedColumnName}', is = '{columnName}'");
                }
                Type type = pTableMetaData[c - 1].DataType;

                dt.Columns.Add(columnName, type);
            }

            return dt;
        }

        private string GetColumnName(int pExtraLinesRowsToSkip, bool pHasHeaderRow, ExcelWorksheet pWorksheet, int pColNr)
        {
            string result = "";

            var excelCell = pWorksheet.Cells[pExtraLinesRowsToSkip + 1, pColNr].Value;
            if (excelCell != null && pHasHeaderRow)
            {
                result = excelCell.ToString();
            }

            return result;
        }

    }
}
