﻿using System;

namespace Aponeo.Business.GH_Konditionen
{
    /// <summary>
    /// class to save some information for the next worker run
    /// </summary>
    internal class WorkerCache
    {
        public DateTime LastRun { get; set; }
    }
}
