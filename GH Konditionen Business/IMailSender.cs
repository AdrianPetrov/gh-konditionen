﻿namespace Aponeo.Basis
{
    public interface IMailSender
    {
        void Send(string pSenderName, string pSenderAddress, string pRecipient, string pSubject, string pBody);
    }
}
