﻿using Aponeo.Basis;
using Aponeo.Business.GH_Konditionen.DTOs;
using log4net;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace Aponeo.Business.GH_Konditionen
{
    public class ConditionComparison
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private IEnumerable<DtoCondition> _actualConditions;
        private IEnumerable<DtoCondition> _newConditions;

        private Dictionary<int, DtoCondition> _actualConditionDict;
        private Dictionary<int, DtoCondition> _newConditionDict;

        public int NumberActual
        {
            get
            {
                return _actualConditions.Count();
            }
        }

        public int NumberNew
        {
            get
            {
                return _newConditions.Count();
            }
        }

        /// <summary>
        /// Gets a Dictionary with old and new (changed) conditions
        /// <para/>
        /// Dictionary&lt;old,new&gt;
        /// </summary>
        public Dictionary<DtoCondition, DtoCondition> Changes { get; }

        public IEnumerable<DtoCondition> NewConditions
        {
            get
            {
                return _newConditions.Where(c => !_actualConditionDict.ContainsKey(c.Pzn));
            }
        }

        public IEnumerable<DtoCondition> DeletedConditions
        {
            get
            {
                return _actualConditions.Where(c => !_newConditionDict.ContainsKey(c.Pzn));
            }
        }

        public ConditionComparison(IEnumerable<DtoCondition> pActualConditions, IEnumerable<DtoCondition> pNewConditions)
        {
            _actualConditions = pActualConditions;
            _newConditions = pNewConditions;

            _actualConditionDict = _actualConditions.ToDictionary(e => e.Pzn, e => e);
            _newConditionDict = _newConditions.ToDictionary(e => e.Pzn, e => e);

            Changes = new Dictionary<DtoCondition, DtoCondition>();
            foreach (DtoCondition nc in _newConditions.Where(c => _actualConditionDict.ContainsKey(c.Pzn)))
            {
                DtoCondition ac = _actualConditionDict[nc.Pzn];
                if ((ac.Price ?? -1) != (nc.Price ?? -1) ||
                    (ac.Discount ?? -1 ) != (nc.Discount ?? -1))
                {
                    Changes.Add(ac, nc);
                }
            }
        }

        public DataTable GetDataTableNew()
        {
            DataTable dt = new DataTable();

            // header
            dt.Columns.Add("PZN").DataType = Type.GetType("System.Int32");
            dt.Columns.Add("Name").DataType = Type.GetType("System.String");
            dt.Columns.Add("EK").DataType = Type.GetType("System.Decimal");
            dt.Columns.Add("Rabatt").DataType = Type.GetType("System.Decimal");

            // table content
            foreach (DtoCondition c in NewConditions)
            {
                DataRow row = dt.NewRow();
                dt.Rows.Add(row);

                row["PZN"] = c.Pzn;
                row["Name"] = c.Name;

                if (c.Price == null)
                {
                    row["EK"] = DBNull.Value;
                }
                else
                {
                    row["EK"] = c.Price;
                }

                if (c.Discount == null)
                {
                    row["Rabatt"] = DBNull.Value;
                }
                else
                {
                    row["Rabatt"] = c.Discount;
                }
            }

            return dt;
        }

        public DataTable GetDataTableDelete()
        {
            DataTable dt = new DataTable();

            // header
            dt.Columns.Add("PZN").DataType = Type.GetType("System.Int32");
            dt.Columns.Add("Name").DataType = Type.GetType("System.String");

            // table content
            foreach (DtoCondition c in DeletedConditions)
            {
                DataRow row = dt.NewRow();
                dt.Rows.Add(row);

                row["PZN"] = c.Pzn;
                row["Name"] = c.Name;
            }

            return dt;
        }

        public DataTable GetDataTableUpdate()
        {
            DataTable dt = new DataTable();

            // header
            dt.Columns.Add("PZN").DataType = Type.GetType("System.Int32");
            dt.Columns.Add("Name").DataType = Type.GetType("System.String");

            dt.Columns.Add("EK alt").DataType = Type.GetType("System.Decimal");
            dt.Columns.Add("EK neu").DataType = Type.GetType("System.Decimal");
            dt.Columns.Add("Diff").DataType = Type.GetType("System.Decimal");
            dt.Columns.Add("Diff in %").DataType = Type.GetType("System.Decimal");

            dt.Columns.Add("Rabatt alt").DataType = Type.GetType("System.Decimal");
            dt.Columns.Add("Rabatt neu").DataType = Type.GetType("System.Decimal");
            dt.Columns.Add("Diff Rabatt").DataType = Type.GetType("System.Decimal");

            // table content
            foreach (KeyValuePair<DtoCondition, DtoCondition> c in Changes)
            {
                DtoCondition oc = c.Key;
                DtoCondition nc = c.Value;

                DataRow row = dt.NewRow();
                dt.Rows.Add(row);

                row["PZN"] = oc.Pzn;
                row["Name"] = oc.Name;

                if (oc.Price == null)
                {
                    row["EK alt"] = DBNull.Value;
                    row["EK neu"] = DBNull.Value;
                    row["Diff"] = DBNull.Value;
                    row["Diff in %"] = DBNull.Value;
                }
                else
                {
                    row["EK alt"] = oc.Price;
                    row["EK neu"] = nc.Price;
                    decimal? diff = nc.Price - oc.Price;
                    decimal? diffPercentage = oc.Price > 0 ? diff / oc.Price : null;
                    row["Diff"] = diff;
                    row["Diff in %"] = diffPercentage;
                }

                if (oc.Discount == null)
                {
                    row["Rabatt alt"] = DBNull.Value;
                    row["Rabatt neu"] = DBNull.Value;
                    row["Diff Rabatt"] = DBNull.Value;
                }
                else
                {
                    row["Rabatt alt"] = oc.Discount;
                    row["Rabatt neu"] = nc.Discount;
                    row["Diff Rabatt"] = nc.Discount - oc.Discount;
                }
            }

            return dt;
        }

        /// <summary>
        /// creates an Excel with all changes from <see cref="pConditionComparison"/> and uploads it to SharePoint
        /// </summary>
        public byte[] CreateExcelWithChanges()
        {
            _log.Notice("create Excel report with changes");

            using (MemoryStream ms = new MemoryStream())
            {
                using (ExcelPackage xl = new ExcelPackage())
                {
                    CreateSheetsWithFormattedTables(xl);

                    xl.SaveAs(ms);
                    ms.Seek(0, SeekOrigin.Begin);

                    return ms.ToArray();
                }
            }
        }

        private void CreateSheetsWithFormattedTables(ExcelPackage xl)
        {
            ExcelWorksheet sheet;
            DataTable dt;

            // update
            dt = GetDataTableUpdate();
            sheet = CreateSheetWithTable(xl, dt, "Update", true, true);
            if (dt.Rows.Count > 0)
            {
                sheet.Cells[$"C2:E{dt.Rows.Count + 1}"].Style.Numberformat.Format = Constants.CURRENCY_FORMAT;
                sheet.Cells[$"F2:F{dt.Rows.Count + 1}"].Style.Numberformat.Format = Constants.PERCENTAGE_FORMAT;
            }

            // new
            dt = GetDataTableNew();
            sheet = CreateSheetWithTable(xl, dt, "New", true);
            if (dt.Rows.Count > 0)
            {
                sheet.Cells[$"C2:C{dt.Rows.Count + 1}"].Style.Numberformat.Format = Constants.CURRENCY_FORMAT;
            }

            // delete
            dt = GetDataTableDelete();
            sheet = CreateSheetWithTable(xl, dt, "Delete", false);
        }

        private static ExcelWorksheet CreateSheetWithTable(ExcelPackage xl, DataTable dt, string pFormatTableName, bool pSort, bool pSortDescending = false)
        {
            ExcelWorksheet sheet = xl.Workbook.Worksheets.Add(pFormatTableName);

            sheet.Cells["A1"].LoadFromDataTable(dt, true);

            if (dt.Rows.Count > 0)
            {
                // format as Excel Table (Filters, colors for each line, ...)
                string lastColumn = Number2String(dt.Columns.Count - 1);
                using (ExcelRange range = sheet.Cells[$"A1:{lastColumn}{dt.Rows.Count + 1}"])
                {
                    ExcelTableCollection tblcollection = sheet.Tables;
                    ExcelTable table = tblcollection.Add(range, pFormatTableName + "Table");

                    table.ShowHeader = true;
                    table.ShowFilter = true;

                    if (pSort)
                    {
                        // sort (with fixed header => A2)
                        ExcelRange rangeToSort = sheet.Cells[$"A2:{lastColumn}{dt.Rows.Count + 1}"];
                        rangeToSort.Sort(dt.Columns.Count - 1, pSortDescending);
                    }

                    range.AutoFitColumns();
                }
            }

            return sheet;
        }

        private static string Number2String(int pIdx)
        {
            string result = "";
            const string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            if (pIdx >= letters.Length)
            {
                result += letters[pIdx / letters.Length - 1];
            }

            result = letters[pIdx % letters.Length].ToString();

            return result;
        }
    }
}
