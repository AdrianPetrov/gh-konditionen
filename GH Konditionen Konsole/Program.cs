﻿using Aponeo.Basis;
using Aponeo.Business.GH_Konditionen;
using log4net;
using System;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace GH_Konditionen_Konsole
{
    public class Program
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected static void Main(string[] args)
        {
            _log.Notice("Start");

            Worker worker = new Worker();
            worker.RunOnes();

            Console.WriteLine("Press ENTER to quit...");
            Console.ReadLine();
        }

    }
}
