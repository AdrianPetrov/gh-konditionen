-- Es sind nur die INSERTs in #StatischeArtikelInput anzupassen, sonst NICHTS (in diesem Skript)
-- Wenn ein Lieferant komplett rausfliegt aus dem Bestellprozess, m�ssen dessen Konditionen manuell gel�scht werden!

USE APONEO;
GO

DECLARE @Prioritaet INT
SET @Prioritaet = 1;

-- Hier kommen die Einkaufspreise je PZN in eine Temp-Tabelle hinein, die anschlie�end in die richtige Tabelle �bertragen werden.
IF OBJECT_ID('tempdb..#StatischeArtikelInput') IS NOT NULL
BEGIN
    DROP TABLE #StatischeArtikelInput;
END

CREATE TABLE #StatischeArtikelInput (
    PZN INT PRIMARY KEY,
    Verf�gbarkeit INT NOT NULL,
    LieferantID INT NOT NULL,
    RabattwertBasisID INT,
    Rabatt DECIMAL,
    AlternativEk MONEY NULL,
    Einkaufspreis MONEY,
    ListeID INT
);

-- ##########################################################################################################################
-- Diese INSERTs sind anzupassen...
-- TODO:
--   - ersetze Punkt durch nichts (Tausender-Trennzeichen entfernen)
--   - ersetze Komma durch Punkt (Dezimal-Trennzeichen)
--   - ersetze TABs durch Komma
--   - regul�rer Ausdruck: ersetze "^" durch "INSERT INTO #StatischeArtikelInput \(PZN, Verf�gbarkeit, LieferantID, RabattwertBasisID, Rabatt, AlternativEk\) VALUES \("
--   - regul�rer Ausdruck: ersetze "$" durch "\);"
--   - ersetze NULLs:
--          - ersetze ",," durch ",NULL,"
--          - ersetze ",\)" durch ",NULL\)"

INSERT INTO #StatischeArtikelInput (PZN, Verf�gbarkeit, LieferantID, RabattwertBasisID, Rabatt, AlternativEk) VALUES (13860796,8,94,1,23,NULL);

-- ##########################################################################################################################
-- Excel-Pflege-Check

SELECT 'unbekannte PZN' AS Beschreibung
      ,i.PZN
  FROM #StatischeArtikelInput i
        LEFT OUTER JOIN ArtikelstammIdentifikation ai
            ON ai.PZN = i.PZN
 WHERE ai.PZN IS NULL;
 
SELECT 'ung�ltige LieferantID in Excel' AS Beschreibung
      ,a.*
  FROM #StatischeArtikelInput a
        LEFT OUTER JOIN WaWiLieferanten l
            ON l.ID = a.LieferantID
 WHERE l.ID IS NULL;

SELECT DISTINCT 'verwendete Lieferanten in Excel' AS Beschreibung
      ,a.LieferantID
      ,l.Firmenname
  FROM #StatischeArtikelInput a
        INNER JOIN WaWiLieferanten l
            ON l.ID = a.LieferantID;

SELECT 'ung�ltige RabattwertBasisID in Excel' AS Beschreibung
      ,a.*
  FROM #StatischeArtikelInput a
        LEFT OUTER JOIN WaWiRabattwertBasisTyp r
            ON r.ID = a.RabattwertBasisID
 WHERE r.ID IS NULL;

SELECT 'Mehrfachdefinition PZN in Excel' AS Beschreibung
      ,a.PZN
  FROM #StatischeArtikelInput a
 GROUP BY a.PZN
 HAVING COUNT(1) > 1;

SELECT CASE
        WHEN a.RabattwertBasisID IN (1, 2) THEN 'Fehler in Excel: Rabatt muss gepflegt sein, AlternativEk nicht' 
        WHEN a.RabattwertBasisID IN (3) THEN 'Fehler in Excel: AlternativEk muss gepflegt sein, Rabatt nicht' 
       END AS Beschreibung
      ,a.*
  FROM #StatischeArtikelInput a
 WHERE (a.RabattwertBasisID IN (1, 2) AND (Rabatt IS NULL OR AlternativEk IS NOT NULL))
    OR (a.RabattwertBasisID IN (3) AND (Rabatt IS NOT NULL OR AlternativEk IS NULL));

-- ##########################################################################################################################
-- Stammdaten pflegen

-- fehlende LieferantID in WaWiArtikelbezugKonditionenPriorisierung nachtragen
INSERT INTO WaWiArtikelbezugKonditionenPriorisierung (LieferantenID, Prioritaet, ListeName)
SELECT DISTINCT i.LieferantID, @Prioritaet, l.Firmenname
  FROM #StatischeArtikelInput i
        LEFT OUTER JOIN WaWiArtikelbezugKonditionenPriorisierung p
            ON p.LieferantenID = i.LieferantID
        INNER JOIN WaWiLieferanten l
            ON i.LieferantID = l.ID
 WHERE p.ID IS NULL;

-- Einkaufspreise ausrechnen
UPDATE i
   SET Einkaufspreis = CASE
                        WHEN i.RabattwertBasisID = 1
                            THEN ap.Einkaufspreis * (100 - i.Rabatt) / 100 -- TaxeEK
                        WHEN i.RabattwertBasisID = 2
                            THEN ap.Abgabepreis * (100 - i.Rabatt) / 100 -- HAP
                        WHEN i.RabattwertBasisID = 3
                            THEN i.AlternativEk
                       END
      ,ListeID = p.ID
  FROM #StatischeArtikelInput i
        INNER JOIN WaWiArtikelbezugKonditionenPriorisierung p
            ON p.LieferantenID = i.LieferantID
        INNER JOIN ArtikelstammIdentifikation ai
            ON ai.PZN = i.PZN
        INNER JOIN ArtikelstammPreis ap
            ON ap.Artikelnummer = ai.Artikelnummer;

-- ##########################################################################################################################
-- ##########################################################################################################################
-- Konsistenz-Check

-- Anzahl
SELECT 'Anzahl PZN vor Einspielung der neuen Liste' AS Beschreibung
      ,COUNT(1) AS Anzahl
  FROM WaWiArtikelbezugKonditionenBasis
 WHERE ArtikelbezugKonditionenPriorisierungID IN (SELECT DISTINCT ListeID FROM #StatischeArtikelInput WHERE ListeID IS NOT NULL)
UNION ALL
SELECT 'Anzahl PZN nach Einspielung der neuen Liste' AS Beschreibung
      ,COUNT(1) AS Anzahl
  FROM #StatischeArtikelInput;

--   => unwarscheinliche Rabatte
SELECT 'unwahrscheinlich hoher Rabatt auf TaxeEK' AS Beschreibung
      ,a.*
  FROM (SELECT i.PZN
              ,asn.Anzeigename
              ,ap.Einkaufspreis AS TaxeEK
              ,i.Einkaufspreis AS EK
              ,CASE 
                WHEN ap.Einkaufspreis > 0 
                    THEN (1 - i.Einkaufspreis / ap.Einkaufspreis)
               END AS Rabatt
          FROM #StatischeArtikelInput i
                INNER JOIN ArtikelstammIdentifikation ai
                    ON ai.PZN = i.PZN
                INNER JOIN ArtikelstammPreis ap
                    ON ap.Artikelnummer = ai.Artikelnummer
                INNER JOIN ArtikelstammName asn
                    ON asn.Artikelnummer = ai.Artikelnummer) a
 WHERE a.Rabatt > 30
 ORDER BY a.Rabatt DESC;

--   => starke Abweichungen
SELECT 'unwahrscheinlich hohe Abweichung gegen�ber der vorherigen Liste' AS Beschreibung
      ,a.*
  FROM (SELECT i.PZN
              ,asn.Anzeigename
              ,akb.Einkaufspreis    AS "EK vorher"
              ,i.Einkaufspreis      AS "EK nachher"
              ,(i.Einkaufspreis - akb.Einkaufspreis) / akb.Einkaufspreis AS Abweichung
          FROM #StatischeArtikelInput i
                INNER JOIN WaWiArtikelbezugKonditionenBasis akb
                    ON akb.PZN = i.PZN
                INNER JOIN ArtikelstammIdentifikation ai
                    ON ai.PZN = i.PZN
                INNER JOIN ArtikelstammName asn
                    ON asn.Artikelnummer = ai.Artikelnummer
         WHERE akb.ArtikelbezugKonditionenPriorisierungID = i.ListeID) a
 WHERE Abweichung > 20;

--   => �nderungen Vorher-Nachher
SELECT 'Unterschiede vorher-nachher' AS Beschreibung
      ,a.*
  FROM (SELECT i.PZN
              ,asn.Anzeigename
              ,akb.Einkaufspreis    AS "EK vorher"
              ,i.Einkaufspreis      AS "EK nachher"
              ,(i.Einkaufspreis - akb.Einkaufspreis) / akb.Einkaufspreis AS Abweichung
          FROM #StatischeArtikelInput i
                INNER JOIN WaWiArtikelbezugKonditionenBasis akb
                    ON akb.PZN = i.PZN
                INNER JOIN ArtikelstammIdentifikation ai
                    ON ai.PZN = i.PZN
                INNER JOIN ArtikelstammName asn
                    ON asn.Artikelnummer = ai.Artikelnummer
         WHERE akb.ArtikelbezugKonditionenPriorisierungID = i.ListeID) a
 WHERE [EK vorher] <> [EK nachher]
 ORDER BY ([EK vorher] - [EK nachher]) / [EK nachher];
 
SELECT 'unbekannte PZN' AS Beschreibung
      ,i.PZN
  FROM #StatischeArtikelInput i
 WHERE i.ListeID IS NULL;

-- ##########################################################################################################################
-- zuerst alles l�schen dieses Lieferanten, dann gesamte Liste neu einspielen
-- Wenn ein Lieferant komplett rausfliegt aus dem Bestellprozess, m�ssen dessen Konditionen und Verf�gbarkeiten manuell gel�scht werden.

-- alle Artikel der manuellen Lieferanten werden zuerst deaktiviert ...
UPDATE v
   SET Verfuegbarkeit = 0
      ,Online = 0
      ,Gehe = 0
      ,Phoenix = 0
      ,Sanacorp = 0
      ,Aep = 0
  FROM PZN_Verfuegbarkeit v
        INNER JOIN WaWiArtikelbezugKonditionenBasis kb
            ON v.PZN = kb.PZN
 WHERE kb.ArtikelbezugKonditionenPriorisierungID IN (SELECT DISTINCT ListeID FROM #StatischeArtikelInput WHERE ListeID IS NOT NULL);

-- ... und deren Konditionen gel�scht
DELETE 
  FROM WaWiArtikelbezugKonditionenBasis
 WHERE ArtikelbezugKonditionenPriorisierungID IN (SELECT DISTINCT ListeID FROM #StatischeArtikelInput WHERE ListeID IS NOT NULL);

-- Import aller Werte
INSERT INTO WaWiArtikelbezugKonditionenBasis (PZN, ArtikelbezugKonditionenPriorisierungID, Einkaufspreis)
SELECT i.PZN
      ,i.ListeID
      ,i.Einkaufspreis
  FROM #StatischeArtikelInput i
 WHERE i.ListeID IS NOT NULL;

-- Check
SELECT COUNT(1) AS Gesamt
      ,COUNT(DISTINCT PZN) AS UniquePZN
  FROM WaWiArtikelbezugKonditionenBasis
 WHERE ArtikelbezugKonditionenPriorisierungID = (SELECT DISTINCT ListeID FROM #StatischeArtikelInput WHERE ListeID IS NOT NULL);

-- Verf�gbarkeit aktualisieren
UPDATE v
   SET Verfuegbarkeit = i.Verf�gbarkeit
      ,Online = 1
  FROM PZN_Verfuegbarkeit v
        INNER JOIN #StatischeArtikelInput i
            ON i.PZN = v.PZN;

-- Verf�gbarkeiten synchronisieren
EXEC dbo.WaWiVerfuegbarkeitAbgleichen;

-- diverse Checks zur Kontrolle
SELECT COUNT(1) AS CountManuelleVorgaben
  FROM WaWiArtikelbezugLieferantenVorgabeManuell;

SELECT 'ArtikelstammVerfuegbarkeit'
      ,av.Verfuegbarkeit
      ,MIN(avs.Name) AS VerfuegbarkeitName
      ,COUNT(1) AS Anzahl_ArtikelstammVerfuegbarkeit
      ,MIN(avs.Beschreibung) AS Beschreibung
  FROM ArtikelstammVerfuegbarkeit av
        INNER JOIN ArtikelstammVerfuegbarkeitStatus avs
            ON avs.ID = av.Verfuegbarkeit
 GROUP BY Verfuegbarkeit;

SELECT 'PZN_Verfuegbarkeit'
      ,Verfuegbarkeit
      ,MIN(avs.Name) AS VerfuegbarkeitName
      ,COUNT(1) AS Anzahl_PZN_Verfuegbarkeit
      ,MIN(avs.Beschreibung) AS Beschreibung
  FROM PZN_Verfuegbarkeit p
        INNER JOIN ArtikelstammVerfuegbarkeitStatus avs
            ON avs.ID = p.Verfuegbarkeit
 GROUP BY Verfuegbarkeit;

SELECT 'WaWiVerfuegbarkeit'
      ,Verfuegbarkeit
      ,MIN(avs.Name) AS VerfuegbarkeitName
      ,COUNT(1) AS Anzahl_WaWiVerfuegbarkeit
      ,MIN(avs.Beschreibung) AS Beschreibung
  FROM WaWiVerfuegbarkeit p
        INNER JOIN ArtikelstammVerfuegbarkeitStatus avs
            ON avs.ID = p.Verfuegbarkeit
 GROUP BY Verfuegbarkeit;

-- SELECT * FROM WaWiArtikelbezugKonditionenPriorisierung;

SELECT 'WaWiArtikelbezugKonditionenBasis'
      ,kb.ArtikelbezugKonditionenPriorisierungID
      ,MIN(kp.ListeName) AS ListeName
      ,COUNT(1) AS Anzahl_Datens�tze
  FROM WaWiArtikelbezugKonditionenBasis kb
        INNER JOIN WaWiArtikelbezugKonditionenPriorisierung kp
            ON kp.ID = kb.ArtikelbezugKonditionenPriorisierungID
 GROUP BY kb.ArtikelbezugKonditionenPriorisierungID
 ORDER BY kb.ArtikelbezugKonditionenPriorisierungID;

SELECT 'WaWiArtikelbezugLieferantenPrioritaet'
      ,lp.LieferantenID
      ,MIN(l.Firmenname) AS LieferantenName
      ,COUNT(1) AS Anzahl_Bestpreise
  FROM WaWiArtikelbezugLieferantenPrioritaet lp
        INNER JOIN WaWiLieferanten l
            ON l.ID = lp.LieferantenID
 WHERE lp.Bestellreihenfolge = 1
 GROUP BY lp.LieferantenID
 ORDER BY lp.LieferantenID;

SELECT 'Import efolgreich'