-- Es sind nur die INSERTs in #NowedaInput anzupassen, sonst NICHTS
DECLARE @ListeID INT
SET @ListeID = 25;

-- Hier kommen die Einkaufspreise je PZN in eine Temp-Tabelle hinein.
IF OBJECT_ID('tempdb..#NowedaInput') IS NOT NULL
BEGIN
    DROP TABLE #NowedaInput;
END
CREATE TABLE #NowedaInput (PZN INT PRIMARY KEY, Einkaufspreis MONEY, ListeID INT);

-- ##########################################################################################################################
-- Diese INSERTs sind anzupassen...
-- TODO:
--   - erste (G�ltigkeitszeitraum) und letzte Zeile (Checksumme #Datens�tze) pr�fen und entfernen
--   - regul�rer Ausdruck: 
--     ersetze "^200000000.+\r\n"                    durch   ""     (ung�ltige Zeilen entfernen: unbekannte PZN)
--     ersetze "^2([0-9]{8}).{54}1([0-9]{6}).+$"     durch   "INSERT INTO #NowedaInput \(PZN, Einkaufspreis\) VALUES \(\1, \2\);"
--     ersetze "^2.+\r\n"                            durch   ""     (ung�ltige Zeilen entfernen)

INSERT INTO #NowedaInput (PZN, Einkaufspreis) VALUES (01437555, 000325);
INSERT INTO #NowedaInput (PZN, Einkaufspreis) VALUES (01870499, 002439);

-- ##########################################################################################################################
-- Einkaufspreise ausrechnen
UPDATE i
   SET ListeID = @ListeID
      --,Einkaufspreis = Einkaufspreis / 100.0      -- @Mario: Falls es in meinem Urlaub Probleme geben sollte, einfach diese Zeile wieder verwenden statt der unteren. Das hier war die Originalzeile vor dem 29.08.
      ,Einkaufspreis = Einkaufspreis / 100.0 * 0.96 -- 4% Rabatt auf die Angebotsliste
  FROM #NowedaInput i;

-- PZN mit Preis 0� entfernen, sind wahrscheinlich neue PZN
DELETE FROM #NowedaInput
 WHERE Einkaufspreis = 0;

-- ##########################################################################################################################
-- ##########################################################################################################################
-- Konsistenz-Check

-- Anzahl
SELECT 'Anzahl PZN vor Einspielung der neuen Liste' AS Beschreibung
      ,COUNT(1) AS Anzahl
  FROM WaWiArtikelbezugKonditionenBasis
 WHERE ArtikelbezugKonditionenPriorisierungID = (SELECT DISTINCT ListeID FROM #NowedaInput)
UNION ALL
SELECT 'Anzahl PZN nach Einspielung der neuen Liste' AS Beschreibung
      ,COUNT(1) AS Anzahl
  FROM #NowedaInput;

--   => �berschneidungen mit anderen Listen desselben Lieferanten
SELECT '�berschneidung PZN mit Liste' AS Beschreibung
      ,i.PZN
      ,asn.Anzeigename
      ,akp2.ListeName
  FROM #NowedaInput i
        INNER JOIN WaWiArtikelbezugKonditionenBasis akb2
            ON akb2.PZN = i.PZN
        INNER JOIN WaWiArtikelbezugKonditionenPriorisierung akp2
            ON akp2.ID <> i.ListeID
           AND akp2.LieferantenID = (SELECT LieferantenID FROM WaWiArtikelbezugKonditionenPriorisierung WHERE ID = (SELECT DISTINCT ListeID FROM #NowedaInput))
           AND akp2.ID = akb2.ArtikelbezugKonditionenPriorisierungID
        INNER JOIN ArtikelstammIdentifikation ai
            ON ai.PZN = i.PZN
        INNER JOIN ArtikelstammName asn
            ON asn.Artikelnummer = ai.Artikelnummer
 ORDER BY akp2.ListeName, i.PZN;

--   => unwarscheinliche Rabatte
SELECT 'unwahrscheinlich hoher Rabatt auf TaxeEK' AS Beschreibung
      ,a.*
  FROM (SELECT i.PZN
              ,asn.Anzeigename
              ,ap.Einkaufspreis     AS TaxeEK
              ,i.Einkaufspreis      AS EK
              ,CASE 
                WHEN ap.Einkaufspreis > 0 
                    THEN (1 - i.Einkaufspreis / ap.Einkaufspreis)
               END AS Rabatt
          FROM #NowedaInput i
                INNER JOIN ArtikelstammIdentifikation ai
                    ON ai.PZN = i.PZN
                INNER JOIN ArtikelstammPreis ap
                    ON ap.Artikelnummer = ai.Artikelnummer
                INNER JOIN ArtikelstammName asn
                    ON asn.Artikelnummer = ai.Artikelnummer) a
 WHERE a.Rabatt > 30
 ORDER BY a.Rabatt DESC;

--   => starke Abweichungen
SELECT 'unwahrscheinlich hohe Abweichung gegen�ber der vorherigen Liste' AS Beschreibung
      ,a.*
  FROM (SELECT i.PZN
              ,asn.Anzeigename
              ,akb.Einkaufspreis    AS "EK vorher"
              ,i.Einkaufspreis      AS "EK nachher"
              ,(i.Einkaufspreis - akb.Einkaufspreis) / akb.Einkaufspreis AS Abweichung
          FROM #NowedaInput i
                INNER JOIN WaWiArtikelbezugKonditionenBasis akb
                    ON akb.PZN = i.PZN
                INNER JOIN ArtikelstammIdentifikation ai
                    ON ai.PZN = i.PZN
                INNER JOIN ArtikelstammName asn
                    ON asn.Artikelnummer = ai.Artikelnummer
         WHERE akb.ArtikelbezugKonditionenPriorisierungID = i.ListeID) a
 WHERE Abweichung > 20;
 
--   => �nderungen Vorher-Nachher
SELECT 'Unterschiede vorher-nachher' AS Beschreibung
      ,a.*
  FROM (SELECT i.PZN
              ,asn.Anzeigename
              ,akb.Einkaufspreis    AS "EK vorher"
              ,i.Einkaufspreis      AS "EK nachher"
              ,(i.Einkaufspreis - akb.Einkaufspreis) / akb.Einkaufspreis AS Abweichung
          FROM #NowedaInput i
                INNER JOIN WaWiArtikelbezugKonditionenBasis akb
                    ON akb.PZN = i.PZN
                INNER JOIN ArtikelstammIdentifikation ai
                    ON ai.PZN = i.PZN
                INNER JOIN ArtikelstammName asn
                    ON asn.Artikelnummer = ai.Artikelnummer
         WHERE akb.ArtikelbezugKonditionenPriorisierungID = i.ListeID) a
 WHERE [EK vorher] <> [EK nachher]
 ORDER BY ([EK vorher] - [EK nachher]) / [EK nachher];


SELECT 'unbekannte PZN' AS Beschreibung
      ,i.PZN
  FROM #NowedaInput i
 WHERE i.ListeID IS NULL;

-- ##########################################################################################################################
-- zuerst alles l�schen, dann gesamte Liste neu einspielen
DELETE kb
-- SELECT kb.*
  FROM WaWiArtikelbezugKonditionenBasis kb
 WHERE kb.ArtikelbezugKonditionenPriorisierungID = (SELECT DISTINCT ListeID FROM #NowedaInput);


-- Import aller Werte f�r alle Sublieferanten
INSERT INTO WaWiArtikelbezugKonditionenBasis (PZN, ArtikelbezugKonditionenPriorisierungID, Einkaufspreis)
SELECT i.PZN
      ,i.ListeID
      ,i.Einkaufspreis
  FROM #NowedaInput i
 WHERE i.ListeID IS NOT NULL;

-- Check
SELECT COUNT(1) AS Gesamt
      ,COUNT(DISTINCT PZN) AS UniquePZN
  FROM WaWiArtikelbezugKonditionenBasis
 WHERE ArtikelbezugKonditionenPriorisierungID = (SELECT DISTINCT ListeID FROM #NowedaInput);

SELECT 'Import efolgreich'
