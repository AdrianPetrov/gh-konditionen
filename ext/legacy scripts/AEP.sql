-- Es sind nur die INSERTs in #AepInput anzupassen, sonst NICHTS
USE APONEO;
GO

DECLARE @ListeID INT
SET @ListeID = 12;

-- Hier kommen die Einkaufspreise je PZN in eine Temp-Tabelle hinein.
IF OBJECT_ID('tempdb..#AepInput') IS NOT NULL
BEGIN
    DROP TABLE #AepInput;
END
CREATE TABLE #AepInput (PZN INT PRIMARY KEY, Einkaufspreis MONEY, ListeID INT);

-- ##########################################################################################################################
-- Diese INSERTs sind anzupassen...
-- TODO:
--   - erste (G�ltigkeitszeitraum) und letzte Zeile (Checksumme #Datens�tze) pr�fen und entfernen
--   - regul�rer Ausdruck: ersetze "^01 0*([0-9]+) [0-9]+ 0*([0-9]+) 001 [0-9]{7} [0-9]{3} [0-9]{7} [0-9]{3} 0 0 0 0 0 0 0 0 0 0$" 
--                         durch   "INSERT INTO #AepInput \(PZN, Einkaufspreis\) VALUES \(\1, \2\);"
--                          => nur zweite und vierte Spalte behalten und jeweils f�hrende Nullen entfernen


INSERT INTO #AepInput (PZN, Einkaufspreis) VALUES (89910101, 259);
INSERT INTO #AepInput (PZN, Einkaufspreis) VALUES (89910102, 167);

-- ##########################################################################################################################

-- Einkaufspreise ausrechnen
UPDATE i
   SET ListeID = @ListeID
      ,Einkaufspreis = Einkaufspreis / 100.0
  FROM #AepInput i;

-- ##########################################################################################################################
-- ##########################################################################################################################
-- Konsistenz-Check

-- Anzahl
SELECT 'Anzahl PZN vor Einspielung der neuen Liste' AS Beschreibung
      ,COUNT(1) AS Anzahl
  FROM WaWiArtikelbezugKonditionenBasis
 WHERE ArtikelbezugKonditionenPriorisierungID = (SELECT DISTINCT ListeID FROM #AepInput)
UNION ALL
SELECT 'Anzahl PZN nach Einspielung der neuen Liste' AS Beschreibung
      ,COUNT(1) AS Anzahl
  FROM #AepInput;

--   => �berschneidungen mit anderen Listen desselben Lieferanten
SELECT '�berschneidung PZN mit Liste' AS Beschreibung
      ,i.PZN
      ,asn.Anzeigename
      ,akp2.ListeName
  FROM #AepInput i
        INNER JOIN WaWiArtikelbezugKonditionenBasis akb2
            ON akb2.PZN = i.PZN
        INNER JOIN WaWiArtikelbezugKonditionenPriorisierung akp2
            ON akp2.ID <> i.ListeID
           AND akp2.LieferantenID = (SELECT LieferantenID FROM WaWiArtikelbezugKonditionenPriorisierung WHERE ID = (SELECT DISTINCT ListeID FROM #AepInput))
           AND akp2.ID = akb2.ArtikelbezugKonditionenPriorisierungID
        INNER JOIN ArtikelstammIdentifikation ai
            ON ai.PZN = i.PZN
        INNER JOIN ArtikelstammName asn
            ON asn.Artikelnummer = ai.Artikelnummer
 ORDER BY akp2.ListeName, i.PZN;

--   => unwarscheinliche Rabatte
SELECT 'unwahrscheinlich hoher Rabatt auf TaxeEK' AS Beschreibung
      ,a.*
  FROM (SELECT i.PZN
              ,asn.Anzeigename
              ,ap.Einkaufspreis     AS TaxeEK
              ,i.Einkaufspreis      AS EK
              ,CASE 
                WHEN ap.Einkaufspreis > 0 
                    THEN (1 - i.Einkaufspreis / ap.Einkaufspreis)
               END AS Rabatt
          FROM #AepInput i
                INNER JOIN ArtikelstammIdentifikation ai
                    ON ai.PZN = i.PZN
                INNER JOIN ArtikelstammPreis ap
                    ON ap.Artikelnummer = ai.Artikelnummer
                INNER JOIN ArtikelstammName asn
                    ON asn.Artikelnummer = ai.Artikelnummer) a
 WHERE a.Rabatt > 30
 ORDER BY a.Rabatt DESC;

--   => starke Abweichungen
SELECT 'unwahrscheinlich hohe Abweichung gegen�ber der vorherigen Liste' AS Beschreibung
      ,a.*
  FROM (SELECT i.PZN
              ,asn.Anzeigename
              ,akb.Einkaufspreis    AS "EK vorher"
              ,i.Einkaufspreis      AS "EK nachher"
              ,(i.Einkaufspreis - akb.Einkaufspreis) / akb.Einkaufspreis AS Abweichung
          FROM #AepInput i
                INNER JOIN WaWiArtikelbezugKonditionenBasis akb
                    ON akb.PZN = i.PZN
                INNER JOIN ArtikelstammIdentifikation ai
                    ON ai.PZN = i.PZN
                INNER JOIN ArtikelstammName asn
                    ON asn.Artikelnummer = ai.Artikelnummer
         WHERE akb.ArtikelbezugKonditionenPriorisierungID = i.ListeID) a
 WHERE Abweichung > 20;
 
--   => �nderungen Vorher-Nachher
SELECT 'Unterschiede vorher-nachher' AS Beschreibung
      ,a.*
  FROM (SELECT i.PZN
              ,asn.Anzeigename
              ,akb.Einkaufspreis    AS "EK vorher"
              ,i.Einkaufspreis      AS "EK nachher"
              ,(i.Einkaufspreis - akb.Einkaufspreis) / akb.Einkaufspreis AS Abweichung
          FROM #AepInput i
                INNER JOIN WaWiArtikelbezugKonditionenBasis akb
                    ON akb.PZN = i.PZN
                INNER JOIN ArtikelstammIdentifikation ai
                    ON ai.PZN = i.PZN
                INNER JOIN ArtikelstammName asn
                    ON asn.Artikelnummer = ai.Artikelnummer
         WHERE akb.ArtikelbezugKonditionenPriorisierungID = i.ListeID) a
 WHERE [EK vorher] <> [EK nachher]
 ORDER BY ([EK vorher] - [EK nachher]) / [EK nachher];


SELECT 'unbekannte PZN' AS Beschreibung
      ,i.PZN
  FROM #AepInput i
 WHERE i.ListeID IS NULL;

-- ##########################################################################################################################
-- zuerst alles l�schen, dann gesamte Liste neu einspielen
DELETE kb
-- SELECT kb.*
  FROM WaWiArtikelbezugKonditionenBasis kb
 WHERE kb.ArtikelbezugKonditionenPriorisierungID = (SELECT DISTINCT ListeID FROM #AepInput);

-- Import aller Werte f�r alle Sublieferanten
INSERT INTO WaWiArtikelbezugKonditionenBasis (PZN, ArtikelbezugKonditionenPriorisierungID, Einkaufspreis)
SELECT i.PZN
      ,i.ListeID
      ,i.Einkaufspreis
  FROM #AepInput i
 WHERE i.ListeID IS NOT NULL;

-- Check
SELECT COUNT(1) AS Gesamt
      ,COUNT(DISTINCT PZN) AS UniquePZN
  FROM WaWiArtikelbezugKonditionenBasis
 WHERE ArtikelbezugKonditionenPriorisierungID = (SELECT DISTINCT ListeID FROM #AepInput);

SELECT 'Import efolgreich'