-- Es sind nur die INSERTs in #GeheGwcInput anzupassen, sonst NICHTS

USE APONEO;
GO

DECLARE @ListeID INT
SET @ListeID = 4;

-- Hier kommen die GWCe je PZN in eine Temp-Tabelle hinein, die anschlie�end f�r alle GEHE-Lieferanten (inkl. Verbund) in die richtige Tabelle �bertragen werden.
IF OBJECT_ID('tempdb..#GeheGwcInput') IS NOT NULL
BEGIN
    DROP TABLE #GeheGwcInput;
END
CREATE TABLE #GeheGwcInput (PZN INT PRIMARY KEY, GWC MONEY NOT NULL, Einkaufspreis MONEY, ListeID INT);

-- ##########################################################################################################################
-- Diese INSERTs sind anzupassen...
-- TODO:
--   - Zeilen entfernen, deren "1. Staffel (Menge)" > 1 ist
--   - ersetze Punkt durch nichts (Tausender-Trennzeichen entfernen)
--   - ersetze Komma durch Punkt (Dezimal-Trennzeichen)
--   - regul�rer Ausdruck: ersetze "^([0-9]+)\t([0-9\.]+)$" 
--                         durch "INSERT INTO #GeheGwcInput \(PZN, GWC\) VALUES \(\1,\2\);"

INSERT INTO #GeheGwcInput (PZN, GWC) VALUES (8884487,17.26);

-- ##########################################################################################################################

-- Einkaufspreise ausrechnen
UPDATE i
   SET Einkaufspreis = i.GWC * 0.99
      ,ListeID = @ListeID
  FROM #GeheGwcInput i
        INNER JOIN WaWiGrossHandelsRabatt ghr
            ON ghr.LieferantenID = 2;

-- ##########################################################################################################################
-- ##########################################################################################################################
-- Konsistenz-Check

-- Anzahl
SELECT 'Anzahl PZN vor Einspielung der neuen Liste' AS Beschreibung
      ,COUNT(1) AS Anzahl
  FROM WaWiArtikelbezugKonditionenBasis
 WHERE ArtikelbezugKonditionenPriorisierungID = (SELECT DISTINCT ListeID FROM #GeheGwcInput WHERE ListeID IS NOT NULL)
UNION ALL
SELECT 'Anzahl PZN nach Einspielung der neuen Liste' AS Beschreibung
      ,COUNT(1) AS Anzahl
  FROM #GeheGwcInput;

--   => unwarscheinliche Rabatte
SELECT 'unwahrscheinlich hoher Rabatt auf TaxeEK' AS Beschreibung
      ,a.*
  FROM (SELECT i.PZN
              ,asn.Anzeigename
              ,ap.Einkaufspreis AS TaxeEK
              ,i.Einkaufspreis    AS EK
              ,CASE 
                WHEN ap.Einkaufspreis > 0 
                    THEN (1 - i.Einkaufspreis / ap.Einkaufspreis)
               END AS Rabatt
          FROM #GeheGwcInput i
                INNER JOIN WaWiGrossHandelsRabatt ghr
                    ON ghr.LieferantenID = 2
                INNER JOIN ArtikelstammIdentifikation ai
                    ON ai.PZN = i.PZN
                INNER JOIN ArtikelstammPreis ap
                    ON ap.Artikelnummer = ai.Artikelnummer
                INNER JOIN ArtikelstammName asn
                    ON asn.Artikelnummer = ai.Artikelnummer) a
 WHERE a.Rabatt > 30
 ORDER BY a.Rabatt DESC;

--   => starke Abweichungen
SELECT 'unwahrscheinlich hohe Abweichung gegen�ber der vorherigen Liste' AS Beschreibung
      ,a.*
  FROM (SELECT i.PZN
              ,asn.Anzeigename
              ,akb.Einkaufspreis    AS "EK vorher"
              ,i.Einkaufspreis      AS "EK nachher"
              ,(i.Einkaufspreis - akb.Einkaufspreis) / akb.Einkaufspreis AS Abweichung
          FROM #GeheGwcInput i
                INNER JOIN WaWiArtikelbezugKonditionenBasis akb
                    ON akb.PZN = i.PZN
                INNER JOIN ArtikelstammIdentifikation ai
                    ON ai.PZN = i.PZN
                INNER JOIN ArtikelstammName asn
                    ON asn.Artikelnummer = ai.Artikelnummer
         WHERE akb.ArtikelbezugKonditionenPriorisierungID = i.ListeID) a
 WHERE Abweichung > 20;

--   => �nderungen Vorher-Nachher
SELECT 'Unterschiede vorher-nachher' AS Beschreibung
      ,a.*
  FROM (SELECT i.PZN
              ,asn.Anzeigename
              ,akb.Einkaufspreis    AS "EK vorher"
              ,i.Einkaufspreis      AS "EK nachher"
              ,(i.Einkaufspreis - akb.Einkaufspreis) / akb.Einkaufspreis AS Abweichung
          FROM #GeheGwcInput i
                INNER JOIN WaWiArtikelbezugKonditionenBasis akb
                    ON akb.PZN = i.PZN
                INNER JOIN ArtikelstammIdentifikation ai
                    ON ai.PZN = i.PZN
                INNER JOIN ArtikelstammName asn
                    ON asn.Artikelnummer = ai.Artikelnummer
         WHERE akb.ArtikelbezugKonditionenPriorisierungID = i.ListeID) a
 WHERE [EK vorher] <> [EK nachher]
 ORDER BY ([EK vorher] - [EK nachher]) / [EK nachher];
  
SELECT 'unbekannte PZN' AS Beschreibung
      ,i.PZN
  FROM #GeheGwcInput i
 WHERE i.ListeID IS NULL;

-- ##########################################################################################################################
-- zuerst alles l�schen, dann gesamte Liste neu einspielen
DELETE
  FROM WaWiArtikelbezugKonditionenBasis
 WHERE ArtikelbezugKonditionenPriorisierungID = (SELECT DISTINCT ListeID FROM #GeheGwcInput WHERE ListeID IS NOT NULL);

-- Import aller Werte
INSERT INTO WaWiArtikelbezugKonditionenBasis (PZN, ArtikelbezugKonditionenPriorisierungID, Einkaufspreis)
SELECT i.PZN, i.ListeID, i.Einkaufspreis
  FROM #GeheGwcInput i;
  
-- Check
SELECT 'Anzahl PZN nach Einspielung der neuen Liste' AS Beschreibung
      ,COUNT(1) AS Gesamt
      ,COUNT(DISTINCT PZN) AS UniquePZN
  FROM WaWiArtikelbezugKonditionenBasis
 WHERE ArtikelbezugKonditionenPriorisierungID = (SELECT DISTINCT ListeID FROM #GeheGwcInput WHERE ListeID IS NOT NULL);

SELECT 'Import efolgreich'