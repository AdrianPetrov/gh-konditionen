-- Es sind nur die INSERTs in #AhInput anzupassen, sonst NICHTS
DECLARE @ListeID INT
SET @ListeID = 20;

-- Hier kommen die Einkaufspreise je PZN in eine Temp-Tabelle hinein.
IF OBJECT_ID('tempdb..#AhInput') IS NOT NULL
BEGIN
    DROP TABLE #AhInput;
END
CREATE TABLE #AhInput (PZN INT PRIMARY KEY, Einkaufspreis MONEY, ListeID INT);

-- ##########################################################################################################################
-- Diese INSERTs sind anzupassen...
-- TODO:
--   - es sind nur die Spalten Material (PZN) und SAEP notwendig, der Rest kann gelöscht werden
--   - regulärer Ausdruck: 
--     ersetze "^([0-9]+)\t([0-9]+),([0-9]+)$"      durch   "INSERT INTO #AhInput \(PZN, Einkaufspreis\) VALUES \(\1, \2\.\3\);"
--     ersetze "^([0-9]+)\t([0-9]+)$"               durch   "INSERT INTO #AhInput \(PZN, Einkaufspreis\) VALUES \(\1, \2\);"

INSERT INTO #AhInput (PZN, Einkaufspreis) VALUES (15639558, 27.4079);
INSERT INTO #AhInput (PZN, Einkaufspreis) VALUES (15743126, 15.7689);

-- ##########################################################################################################################

-- Einkaufspreise ausrechnen
UPDATE i
   SET ListeID = @ListeID
  FROM #AhInput i;

-- PZN mit Preis 0€ entfernen, sind wahrscheinlich neue PZN
DELETE 
--SELECT *
  FROM #AhInput
 WHERE Einkaufspreis = 0;

-- ##########################################################################################################################
-- ##########################################################################################################################
-- Konsistenz-Check

-- Anzahl
SELECT 'Anzahl PZN vor Einspielung der neuen Liste' AS Beschreibung
      ,COUNT(1) AS Anzahl
  FROM WaWiArtikelbezugKonditionenBasis
 WHERE ArtikelbezugKonditionenPriorisierungID = (SELECT DISTINCT ListeID FROM #AhInput)
UNION ALL
SELECT 'Anzahl PZN nach Einspielung der neuen Liste' AS Beschreibung
      ,COUNT(1) AS Anzahl
  FROM #AhInput;

--   => Überschneidungen mit anderen Listen desselben Lieferanten
SELECT 'Überschneidung PZN mit Liste' AS Beschreibung
      ,i.PZN
      ,asn.Anzeigename
      ,akp2.ListeName
  FROM #AhInput i
        INNER JOIN WaWiArtikelbezugKonditionenBasis akb2
            ON akb2.PZN = i.PZN
        INNER JOIN WaWiArtikelbezugKonditionenPriorisierung akp2
            ON akp2.ID <> i.ListeID
           AND akp2.LieferantenID = (SELECT LieferantenID FROM WaWiArtikelbezugKonditionenPriorisierung WHERE ID = (SELECT DISTINCT ListeID FROM #AhInput))
           AND akp2.ID = akb2.ArtikelbezugKonditionenPriorisierungID
        INNER JOIN ArtikelstammIdentifikation ai
            ON ai.PZN = i.PZN
        INNER JOIN ArtikelstammName asn
            ON asn.Artikelnummer = ai.Artikelnummer
 ORDER BY akp2.ListeName, i.PZN;

--   => unwarscheinliche Rabatte
SELECT 'unwahrscheinlich hoher Rabatt auf TaxeEK' AS Beschreibung
      ,a.*
  FROM (SELECT i.PZN
              ,asn.Anzeigename
              ,ap.Einkaufspreis     AS TaxeEK
              ,i.Einkaufspreis      AS EK
              ,CASE 
                WHEN ap.Einkaufspreis > 0 
                    THEN (1 - i.Einkaufspreis / ap.Einkaufspreis)
               END AS Rabatt
          FROM #AhInput i
                INNER JOIN ArtikelstammIdentifikation ai
                    ON ai.PZN = i.PZN
                INNER JOIN ArtikelstammPreis ap
                    ON ap.Artikelnummer = ai.Artikelnummer
                INNER JOIN ArtikelstammName asn
                    ON asn.Artikelnummer = ai.Artikelnummer) a
 WHERE a.Rabatt > 30
 ORDER BY a.Rabatt DESC;

--   => starke Abweichungen
SELECT 'unwahrscheinlich hohe Abweichung gegenüber der vorherigen Liste' AS Beschreibung
      ,a.*
  FROM (SELECT i.PZN
              ,asn.Anzeigename
              ,akb.Einkaufspreis    AS "EK vorher"
              ,i.Einkaufspreis      AS "EK nachher"
              ,(i.Einkaufspreis - akb.Einkaufspreis) / akb.Einkaufspreis AS Abweichung
          FROM #AhInput i
                INNER JOIN WaWiArtikelbezugKonditionenBasis akb
                    ON akb.PZN = i.PZN
                INNER JOIN ArtikelstammIdentifikation ai
                    ON ai.PZN = i.PZN
                INNER JOIN ArtikelstammName asn
                    ON asn.Artikelnummer = ai.Artikelnummer
         WHERE akb.ArtikelbezugKonditionenPriorisierungID = i.ListeID) a
 WHERE Abweichung > 20;
 
--   => Änderungen Vorher-Nachher
SELECT 'Unterschiede vorher-nachher' AS Beschreibung
      ,a.*
  FROM (SELECT i.PZN
              ,asn.Anzeigename
              ,akb.Einkaufspreis    AS "EK vorher"
              ,i.Einkaufspreis      AS "EK nachher"
              ,(i.Einkaufspreis - akb.Einkaufspreis) / akb.Einkaufspreis AS Abweichung
          FROM #AhInput i
                INNER JOIN WaWiArtikelbezugKonditionenBasis akb
                    ON akb.PZN = i.PZN
                INNER JOIN ArtikelstammIdentifikation ai
                    ON ai.PZN = i.PZN
                INNER JOIN ArtikelstammName asn
                    ON asn.Artikelnummer = ai.Artikelnummer
         WHERE akb.ArtikelbezugKonditionenPriorisierungID = i.ListeID) a
 WHERE [EK vorher] <> [EK nachher]
 ORDER BY ([EK vorher] - [EK nachher]) / [EK nachher];


SELECT 'unbekannte PZN' AS Beschreibung
      ,i.PZN
  FROM #AhInput i
 WHERE i.ListeID IS NULL;

-- ##########################################################################################################################
-- zuerst alles löschen, dann gesamte Liste neu einspielen
DELETE kb
-- SELECT kb.*
  FROM WaWiArtikelbezugKonditionenBasis kb
 WHERE kb.ArtikelbezugKonditionenPriorisierungID = (SELECT DISTINCT ListeID FROM #AhInput);


-- Import aller Werte für alle Sublieferanten
INSERT INTO WaWiArtikelbezugKonditionenBasis (PZN, ArtikelbezugKonditionenPriorisierungID, Einkaufspreis)
SELECT i.PZN
      ,i.ListeID
      ,i.Einkaufspreis
  FROM #AhInput i
 WHERE i.ListeID IS NOT NULL;

-- Check
SELECT COUNT(1) AS Gesamt
      ,COUNT(DISTINCT PZN) AS UniquePZN
  FROM WaWiArtikelbezugKonditionenBasis
 WHERE ArtikelbezugKonditionenPriorisierungID = (SELECT DISTINCT ListeID FROM #AhInput);

SELECT 'Import efolgreich'