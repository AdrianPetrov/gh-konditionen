-- Es sind nur die INSERTs in #SanacorpMinderspannenInputTmp anzupassen, sonst NICHTS

USE APONEO;
GO

DECLARE @ListeID INT
SET @ListeID = 9;

-- Hier kommen die Einkaufspreise je PZN in eine Temp-Tabelle hinein, die anschlie�end f�r alle Sanacorp-Lieferanten (inkl. Verbund) in die richtige Tabelle �bertragen werden.
-- Rohdaten-Input
IF OBJECT_ID('tempdb..#SanacorpMinderspannenInputTmp') IS NOT NULL
BEGIN
    DROP TABLE #SanacorpMinderspannenInputTmp;
END
CREATE TABLE #SanacorpMinderspannenInputTmp (PZN INT, Einkaufspreis MONEY NOT NULL);

-- fachliche Selektion aus den Rohdaten
IF OBJECT_ID('tempdb..#SanacorpMinderspannenInput') IS NOT NULL
BEGIN
    DROP TABLE #SanacorpMinderspannenInput;
END
CREATE TABLE #SanacorpMinderspannenInput (PZN INT PRIMARY KEY, Einkaufspreis MONEY NOT NULL, ListeID INT);

/*
   ##########################################################################################################################
   Diese INSERTs sind anzupassen...
   Zusatz-Info:
     - Es gibt doppelte PZN-Eintr�ge mit unterschiedlichen Preisen je nach Bestellmenge. Es ist nur der teurere Preis (mit kleinerer Bestellmenge) 
       zu verwenden. Dies wird in einem zweiten Schritt durchgef�hrt (Selektion des jeweils h�hsten Preises je PZN).
   TODO:
     - Es sind nur die Spalten PZN + STAFFEL_PREIS relevant
     - ersetze Punkt durch nichts (Tausender-Trennzeichen entfernen)
     - ersetze Komma durch Punkt (Dezimal-Trennzeichen)
     - regul�rer Ausdruck: ersetze "^(.+)\t(.+)$"
                           durch "INSERT INTO #SanacorpMinderspannenInputTmp \(PZN, Einkaufspreis\) VALUES \(\1,\2\);"
*/

INSERT INTO #SanacorpMinderspannenInputTmp (PZN, Einkaufspreis) VALUES (721, 21.78);

-- ##########################################################################################################################

INSERT INTO #SanacorpMinderspannenInput (PZN, Einkaufspreis)
SELECT PZN, MAX(Einkaufspreis)
  FROM #SanacorpMinderspannenInputTmp
 GROUP BY PZN;

UPDATE i
   SET ListeID = @ListeID
      ,Einkaufspreis = Einkaufspreis * 0.98 -- 2% Rabatt auf "Sanacorp Sonderpreise"
  FROM #SanacorpMinderspannenInput i;

-- ##########################################################################################################################
-- ##########################################################################################################################
-- Konsistenz-Check

-- Anzahl
SELECT 'Anzahl PZN vor Einspielung der neuen Liste' AS Beschreibung
      ,COUNT(1) AS Anzahl
  FROM WaWiArtikelbezugKonditionenBasis
 WHERE ArtikelbezugKonditionenPriorisierungID = (SELECT DISTINCT ListeID FROM #SanacorpMinderspannenInput WHERE ListeID IS NOT NULL)
UNION ALL
SELECT 'Anzahl PZN nach Einspielung der neuen Liste' AS Beschreibung
      ,COUNT(1) AS Anzahl
  FROM #SanacorpMinderspannenInput;

--   => unwarscheinliche Rabatte
SELECT 'unwahrscheinlich hoher Rabatt auf TaxeEK' AS Beschreibung
      ,a.*
  FROM (SELECT i.PZN
              ,asn.Anzeigename
              ,ap.Einkaufspreis AS TaxeEK
              ,i.Einkaufspreis    AS EK
              ,CASE 
                WHEN ap.Einkaufspreis > 0 
                    THEN (1 - i.Einkaufspreis / ap.Einkaufspreis)
               END AS Rabatt
          FROM #SanacorpMinderspannenInput i
                INNER JOIN WaWiGrossHandelsRabatt ghr
                    ON ghr.LieferantenID = 2
                INNER JOIN ArtikelstammIdentifikation ai
                    ON ai.PZN = i.PZN
                INNER JOIN ArtikelstammPreis ap
                    ON ap.Artikelnummer = ai.Artikelnummer
                INNER JOIN ArtikelstammName asn
                    ON asn.Artikelnummer = ai.Artikelnummer) a
 WHERE a.Rabatt > 30
 ORDER BY a.Rabatt DESC;

--   => starke Abweichungen
SELECT 'unwahrscheinlich hohe Abweichung gegen�ber der vorherigen Liste' AS Beschreibung
      ,a.*
  FROM (SELECT i.PZN
              ,asn.Anzeigename
              ,akb.Einkaufspreis    AS "EK vorher"
              ,i.Einkaufspreis      AS "EK nachher"
              ,(i.Einkaufspreis - akb.Einkaufspreis) / akb.Einkaufspreis AS Abweichung
          FROM #SanacorpMinderspannenInput i
                INNER JOIN WaWiArtikelbezugKonditionenBasis akb
                    ON akb.PZN = i.PZN
                INNER JOIN ArtikelstammIdentifikation ai
                    ON ai.PZN = i.PZN
                INNER JOIN ArtikelstammName asn
                    ON asn.Artikelnummer = ai.Artikelnummer
         WHERE akb.ArtikelbezugKonditionenPriorisierungID = i.ListeID) a
 WHERE Abweichung > 20;
 
--   => �nderungen Vorher-Nachher
SELECT 'Unterschiede vorher-nachher' AS Beschreibung
      ,a.*
  FROM (SELECT i.PZN
              ,asn.Anzeigename
              ,akb.Einkaufspreis    AS "EK vorher"
              ,i.Einkaufspreis      AS "EK nachher"
              ,(i.Einkaufspreis - akb.Einkaufspreis) / akb.Einkaufspreis AS Abweichung
          FROM #SanacorpMinderspannenInput i
                INNER JOIN WaWiArtikelbezugKonditionenBasis akb
                    ON akb.PZN = i.PZN
                INNER JOIN ArtikelstammIdentifikation ai
                    ON ai.PZN = i.PZN
                INNER JOIN ArtikelstammName asn
                    ON asn.Artikelnummer = ai.Artikelnummer
         WHERE akb.ArtikelbezugKonditionenPriorisierungID = i.ListeID) a
 WHERE [EK vorher] <> [EK nachher]
 ORDER BY ([EK vorher] - [EK nachher]) / [EK nachher];

SELECT 'unbekannte PZN' AS Beschreibung
      ,i.PZN
  FROM #SanacorpMinderspannenInput i
 WHERE i.ListeID IS NULL;

-- ##########################################################################################################################
-- zuerst alles l�schen, dann gesamte Liste neu einspielen
DELETE
  FROM WaWiArtikelbezugKonditionenBasis
 WHERE ArtikelbezugKonditionenPriorisierungID = (SELECT DISTINCT ListeID FROM #SanacorpMinderspannenInput WHERE ListeID IS NOT NULL);

-- Import aller Werte f�r alle Sublieferanten
INSERT INTO WaWiArtikelbezugKonditionenBasis (PZN, ArtikelbezugKonditionenPriorisierungID, Einkaufspreis)
SELECT i.PZN, i.ListeID, i.Einkaufspreis
  FROM #SanacorpMinderspannenInput i;

-- Check
SELECT COUNT(1) AS Gesamt
      ,COUNT(DISTINCT PZN) AS UniquePZN
  FROM [APONEO].[dbo].WaWiArtikelbezugKonditionenBasis
 WHERE ArtikelbezugKonditionenPriorisierungID = (SELECT DISTINCT ListeID FROM #SanacorpMinderspannenInput WHERE ListeID IS NOT NULL);

SELECT 'Import efolgreich'