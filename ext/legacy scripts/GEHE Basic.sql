-- Es sind nur die INSERTs in #GeheBasicInput anzupassen, sonst NICHTS

USE APONEO;
GO

DECLARE @ListeID INT
SET @ListeID = 5;

-- Hier kommen die Einkaufspreise je PZN in eine Temp-Tabelle hinein, die anschlie�end f�r alle GEHE-Lieferanten (inkl. Verbund) in die richtige Tabelle �bertragen werden.
IF OBJECT_ID('tempdb..#GeheBasicInput') IS NOT NULL
BEGIN
    DROP TABLE #GeheBasicInput;
END
CREATE TABLE #GeheBasicInput (PZN INT PRIMARY KEY, KonditionsPreis MONEY NOT NULL, Einkaufspreis MONEY, ListeID INT);

-- ##########################################################################################################################
-- Diese INSERTs sind anzupassen...
-- TODO:
--   - ersetze Punkt durch nichts (Tausender-Trennzeichen entfernen)
--   - ersetze Komma durch Punkt (Dezimal-Trennzeichen)
--   - regul�rer Ausdruck: ersetze "^([0-9]+)\t([0-9\.]+)$" 
--                         durch "INSERT INTO #GeheBasicInput \(PZN, KonditionsPreis\) VALUES \(\1,\2\);"

INSERT INTO #GeheBasicInput (PZN, KonditionsPreis) VALUES (170,23.12);

-- ##########################################################################################################################

-- Einkaufspreise ausrechnen
UPDATE i
   SET Einkaufspreis = ap.Einkaufspreis - i.KonditionsPreis * ghr.MinderspannRabatt / 100
      ,ListeID = @ListeID
  FROM #GeheBasicInput i
        INNER JOIN ArtikelstammIdentifikation ai
            ON ai.PZN = i.PZN
        INNER JOIN ArtikelstammPreis ap
            ON ap.Artikelnummer = ai.Artikelnummer
        INNER JOIN WaWiGrossHandelsRabatt ghr
            ON ghr.LieferantenID = 2;

-- ##########################################################################################################################
-- ##########################################################################################################################
-- Konsistenz-Check

-- Anzahl
SELECT 'Anzahl PZN vor Einspielung der neuen Liste' AS Beschreibung
      ,COUNT(1) AS Anzahl
  FROM WaWiArtikelbezugKonditionenBasis
 WHERE ArtikelbezugKonditionenPriorisierungID = (SELECT DISTINCT ListeID FROM #GeheBasicInput WHERE ListeID IS NOT NULL)
UNION ALL
SELECT 'Anzahl PZN nach Einspielung der neuen Liste' AS Beschreibung
      ,COUNT(1) AS Anzahl
  FROM #GeheBasicInput;

--   => unwarscheinliche Rabatte
SELECT 'unwahrscheinlich hoher Rabatt auf TaxeEK' AS Beschreibung
      ,a.*
  FROM (SELECT i.PZN
              ,asn.Anzeigename
              ,ap.Einkaufspreis     AS TaxeEK
              ,i.Einkaufspreis      AS EK
              ,CASE 
                WHEN ap.Einkaufspreis > 0 
                    THEN (1 - i.Einkaufspreis / ap.Einkaufspreis)
               END AS Rabatt
          FROM #GeheBasicInput i
                INNER JOIN ArtikelstammIdentifikation ai
                    ON ai.PZN = i.PZN
                INNER JOIN ArtikelstammPreis ap
                    ON ap.Artikelnummer = ai.Artikelnummer
                INNER JOIN ArtikelstammName asn
                    ON asn.Artikelnummer = ai.Artikelnummer) a
 WHERE a.Rabatt > 30
 ORDER BY a.Rabatt DESC;

--   => starke Abweichungen
SELECT 'unwahrscheinlich hohe Abweichung gegen�ber der vorherigen Liste' AS Beschreibung
      ,a.*
  FROM (SELECT i.PZN
              ,asn.Anzeigename
              ,akb.Einkaufspreis    AS "EK vorher"
              ,i.Einkaufspreis      AS "EK nachher"
              ,(i.Einkaufspreis - akb.Einkaufspreis) / akb.Einkaufspreis AS Abweichung
          FROM #GeheBasicInput i
                INNER JOIN WaWiArtikelbezugKonditionenBasis akb
                    ON akb.PZN = i.PZN
                INNER JOIN ArtikelstammIdentifikation ai
                    ON ai.PZN = i.PZN
                INNER JOIN ArtikelstammName asn
                    ON asn.Artikelnummer = ai.Artikelnummer
         WHERE akb.ArtikelbezugKonditionenPriorisierungID = i.ListeID) a
 WHERE Abweichung > 20;
 
--   => �nderungen Vorher-Nachher
SELECT 'Unterschiede vorher-nachher' AS Beschreibung
      ,a.*
  FROM (SELECT i.PZN
              ,asn.Anzeigename
              ,akb.Einkaufspreis    AS "EK vorher"
              ,i.Einkaufspreis      AS "EK nachher"
              ,(i.Einkaufspreis - akb.Einkaufspreis) / akb.Einkaufspreis AS Abweichung
          FROM #GeheBasicInput i
                INNER JOIN WaWiArtikelbezugKonditionenBasis akb
                    ON akb.PZN = i.PZN
                INNER JOIN ArtikelstammIdentifikation ai
                    ON ai.PZN = i.PZN
                INNER JOIN ArtikelstammName asn
                    ON asn.Artikelnummer = ai.Artikelnummer
         WHERE akb.ArtikelbezugKonditionenPriorisierungID = i.ListeID) a
 WHERE [EK vorher] <> [EK nachher]
 ORDER BY ([EK vorher] - [EK nachher]) / [EK nachher];


SELECT 'unbekannte PZN' AS Beschreibung
      ,i.PZN
  FROM #GeheBasicInput i
 WHERE i.ListeID IS NULL;

-- ##########################################################################################################################
-- zuerst alles l�schen, dann gesamte Liste neu einspielen
DELETE 
-- SELECT *
  FROM WaWiArtikelbezugKonditionenBasis
 WHERE ArtikelbezugKonditionenPriorisierungID = (SELECT DISTINCT ListeID FROM #GeheBasicInput WHERE ListeID IS NOT NULL);

-- Import aller Werte f�r alle Sublieferanten
INSERT INTO WaWiArtikelbezugKonditionenBasis (PZN, ArtikelbezugKonditionenPriorisierungID, Einkaufspreis)
SELECT i.PZN
      ,i.ListeID
      ,i.Einkaufspreis
  FROM #GeheBasicInput i
 WHERE i.ListeID IS NOT NULL;

-- Check
SELECT COUNT(1) AS Gesamt
      ,COUNT(DISTINCT PZN) AS UniquePZN
  FROM WaWiArtikelbezugKonditionenBasis
 WHERE ArtikelbezugKonditionenPriorisierungID = (SELECT DISTINCT ListeID FROM #GeheBasicInput WHERE ListeID IS NOT NULL);

SELECT 'Import efolgreich'