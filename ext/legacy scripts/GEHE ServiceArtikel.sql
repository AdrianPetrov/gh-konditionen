-- Es sind nur die INSERTs in #GeheServiceArtikelInput anzupassen, sonst NICHTS

USE APONEO;
GO

DECLARE @ListeID INT
SET @ListeID = 6;

-- Hier kommen die Einkaufspreise je PZN in eine Temp-Tabelle hinein, die anschlie�end f�r alle GEHE-Lieferanten (inkl. Verbund) in die richtige Tabelle �bertragen werden.
IF OBJECT_ID('tempdb..#GeheServiceArtikelInput') IS NOT NULL
BEGIN
    DROP TABLE #GeheServiceArtikelInput;
END
CREATE TABLE #GeheServiceArtikelInput (PZN INT PRIMARY KEY, Einkaufspreis MONEY, ListeID INT);

-- ##########################################################################################################################
-- Diese INSERTs sind anzupassen...
-- TODO:
--   - regul�rer Ausdruck: ersetze "^([0-9]+)$" durch "INSERT INTO #GeheServiceArtikelInput \(PZN\) VALUES \(\1\);"

INSERT INTO #GeheServiceArtikelInput (PZN) VALUES (150337);


-- ##########################################################################################################################

-- Einkaufspreise ausrechnen
UPDATE i
   SET Einkaufspreis = ap.Einkaufspreis * (100 - ghr.MinderspannRabatt) / 100
      ,ListeID = @ListeID
  FROM #GeheServiceArtikelInput i
        INNER JOIN ArtikelstammIdentifikation ai
            ON ai.PZN = i.PZN
        INNER JOIN ArtikelstammPreis ap
            ON ap.Artikelnummer = ai.Artikelnummer
        INNER JOIN WaWiGrossHandelsRabatt ghr
            ON ghr.LieferantenID = 2;

-- ##########################################################################################################################
-- ##########################################################################################################################
-- Konsistenz-Check

-- Anzahl
SELECT 'Anzahl PZN vor Einspielung der neuen Liste' AS Beschreibung
      ,COUNT(1) AS Anzahl
  FROM WaWiArtikelbezugKonditionenBasis
 WHERE ArtikelbezugKonditionenPriorisierungID = (SELECT DISTINCT ListeID FROM #GeheServiceArtikelInput WHERE ListeID IS NOT NULL)
UNION ALL
SELECT 'Anzahl PZN nach Einspielung der neuen Liste' AS Beschreibung
      ,COUNT(1) AS Anzahl
  FROM #GeheServiceArtikelInput;

--   => unwarscheinliche Rabatte
SELECT 'unwahrscheinlich hoher Rabatt auf TaxeEK' AS Beschreibung
      ,a.*
  FROM (SELECT i.PZN
              ,asn.Anzeigename
              ,ap.Einkaufspreis AS TaxeEK
              ,i.Einkaufspreis    AS EK
              ,CASE 
                WHEN ap.Einkaufspreis > 0 
                    THEN (1 - i.Einkaufspreis / ap.Einkaufspreis)
               END AS Rabatt
          FROM #GeheServiceArtikelInput i
                INNER JOIN WaWiGrossHandelsRabatt ghr
                    ON ghr.LieferantenID = 2
                INNER JOIN ArtikelstammIdentifikation ai
                    ON ai.PZN = i.PZN
                INNER JOIN ArtikelstammPreis ap
                    ON ap.Artikelnummer = ai.Artikelnummer
                INNER JOIN ArtikelstammName asn
                    ON asn.Artikelnummer = ai.Artikelnummer) a
 WHERE a.Rabatt > 30
 ORDER BY a.Rabatt DESC;

--   => starke Abweichungen
SELECT 'unwahrscheinlich hohe Abweichung gegen�ber der vorherigen Liste' AS Beschreibung
      ,a.*
  FROM (SELECT i.PZN
              ,asn.Anzeigename
              ,akb.Einkaufspreis    AS "EK vorher"
              ,i.Einkaufspreis      AS "EK nachher"
              ,(i.Einkaufspreis - akb.Einkaufspreis) / akb.Einkaufspreis AS Abweichung
          FROM #GeheServiceArtikelInput i
                INNER JOIN WaWiArtikelbezugKonditionenBasis akb
                    ON akb.PZN = i.PZN
                INNER JOIN ArtikelstammIdentifikation ai
                    ON ai.PZN = i.PZN
                INNER JOIN ArtikelstammName asn
                    ON asn.Artikelnummer = ai.Artikelnummer
         WHERE akb.ArtikelbezugKonditionenPriorisierungID = i.ListeID) a
 WHERE Abweichung > 20;

--   => �nderungen Vorher-Nachher
SELECT 'Unterschiede vorher-nachher' AS Beschreibung
      ,a.*
  FROM (SELECT i.PZN
              ,asn.Anzeigename
              ,akb.Einkaufspreis    AS "EK vorher"
              ,i.Einkaufspreis      AS "EK nachher"
              ,(i.Einkaufspreis - akb.Einkaufspreis) / akb.Einkaufspreis AS Abweichung
          FROM #GeheServiceArtikelInput i
                INNER JOIN WaWiArtikelbezugKonditionenBasis akb
                    ON akb.PZN = i.PZN
                INNER JOIN ArtikelstammIdentifikation ai
                    ON ai.PZN = i.PZN
                INNER JOIN ArtikelstammName asn
                    ON asn.Artikelnummer = ai.Artikelnummer
         WHERE akb.ArtikelbezugKonditionenPriorisierungID = i.ListeID) a
 WHERE [EK vorher] <> [EK nachher]
 ORDER BY ([EK vorher] - [EK nachher]) / [EK nachher];
 
SELECT 'unbekannte PZN' AS Beschreibung
      ,i.PZN
  FROM #GeheServiceArtikelInput i
 WHERE i.ListeID IS NULL;

-- neue PZN auf der Liste?
WITH alteListe AS
(
    SELECT *
      FROM WaWiArtikelbezugKonditionenBasis kb
     WHERE kb.ArtikelbezugKonditionenPriorisierungID = (SELECT DISTINCT ListeID FROM #GeheServiceArtikelInput WHERE ListeID IS NOT NULL)
)
SELECT *
  FROM #GeheServiceArtikelInput n
        FULL OUTER JOIN alteListe kb
            ON kb.PZN = n.PZN
 WHERE kb.PZN IS NULL
    OR n.PZN IS NULL;

-- ##########################################################################################################################
-- zuerst alles l�schen, dann gesamte Liste neu einspielen
DELETE
  FROM WaWiArtikelbezugKonditionenBasis
 WHERE ArtikelbezugKonditionenPriorisierungID = (SELECT DISTINCT ListeID FROM #GeheServiceArtikelInput WHERE ListeID IS NOT NULL);

-- Import aller Werte f�r alle Sublieferanten
INSERT INTO WaWiArtikelbezugKonditionenBasis (PZN, ArtikelbezugKonditionenPriorisierungID, Einkaufspreis)
SELECT i.PZN
      ,i.ListeID
      ,i.Einkaufspreis
  FROM #GeheServiceArtikelInput i;

-- Check
SELECT COUNT(1) AS Gesamt
      ,COUNT(DISTINCT PZN) AS UniquePZN
  FROM [APONEO].[dbo].WaWiArtikelbezugKonditionenBasis
 WHERE ArtikelbezugKonditionenPriorisierungID = (SELECT DISTINCT ListeID FROM #GeheServiceArtikelInput WHERE ListeID IS NOT NULL);

SELECT 'Import efolgreich'